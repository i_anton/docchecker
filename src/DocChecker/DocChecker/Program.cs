﻿using System;
using System.Windows.Forms;
using DocChecker.Core;
using DocChecker.GUI;

namespace DocChecker
{
	internal static class Program
	{
		/// <summary>
		///    Главная точка входа для приложения.
		/// </summary>
		[STAThread]
		private static void Main()
		{
			Application.ApplicationExit += (sender, e) => DocumentControl.CloseDocument();

			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new FormMain());
		}
	}
}