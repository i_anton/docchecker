﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Json;
using DocChecker.Core.Checks;
using DocChecker.Core.Checks.Parameters;

namespace DocChecker.Core
{
	/// <summary>
	///    Представляет список проверок, интерфейс для взаимодействия с ним и получения результатов.
	/// </summary>
	internal static class CheckList
	{
		private static readonly List<Check> Checks = new List<Check>();

		/// <summary>
		///    Возвращает список всех проверок.
		/// </summary>
		public static IEnumerable<Check> AllChecks => Checks;

		/// <summary>
		///    Добавляет проверку заданного типа в чеклист.
		/// </summary>
		public static Check Add(Type checkType)
		{
			if (!checkType.IsSubclassOf(typeof(Check)))
				throw new ArgumentException("Input type must be derived from Check.");

			var check = (Check) checkType.GetConstructor(Type.EmptyTypes).Invoke(new Object[0]);
			Checks.Add(check);

			CheckAdded?.Invoke(new CheckListEventArgs(check));
			return check;
		}

		public static void Remove(Check check)
		{
			try
			{
				Checks.Remove(check);
				CheckRemoved?.Invoke(new CheckListEventArgs(check));
			}
			catch (KeyNotFoundException)
			{
				throw new ArgumentException("Попытка удалить проверку, отсутствующую в списке.");
			}
		}

		public static void Clear()
		{
			Checks.Clear();
			CheckListCleared?.Invoke(null, EventArgs.Empty);
		}

		public static event CheckListEventHandler CheckRemoved;
		public static event CheckListEventHandler CheckAdded;
		public static event EventHandler CheckListCleared;

		/// <summary>
		///    Вызывает все проверки для указанного документа и возвращает список результатов.
		/// </summary>
		public static IEnumerable<CheckResult> InvokeAll(Tuple<WordDocument, BackgroundWorker> parameters)
		{
			var wordDocument = parameters.Item1;
			var backgroundWorker = parameters.Item2;

			ApplicationLog.LogMessage(
				$"Выбранные проверки запущены для документа \"{Path.GetFileName(DocumentControl.CurrentDocument.Path)}\".");

			var resultList = new List<CheckResult>();
			Int32 number = 0;
			foreach (var check in AllChecks)
			{
				ApplicationLog.LogState($"Проверка: {check.Name}");
				try
				{
					var result = check.Invoke(wordDocument, ref backgroundWorker);
					resultList.Add(result);
					//TODO улучшенная обработка отмены
					number++;
					backgroundWorker.ReportProgress(number * 100 / Checks.Count);

					ApplicationLog.LogResult(result);
					if (backgroundWorker.CancellationPending)
					{
						ApplicationLog.LogMessage(
							$"Проверка документа \"{Path.GetFileName(DocumentControl.CurrentDocument.Path)}\" прервана");
						ApplicationLog.LogState("Прервано");
						backgroundWorker.ReportProgress(100);
						return resultList;
					}
				}
				catch (Exception ex)
				{
					ApplicationLog.LogError(
						$"При выполнении проверки {check.GetType()} произошла ошибка типа {ex.GetType()}:{Environment.NewLine}\"{ex.Message}\"");
				}
			}

			ApplicationLog.LogMessage(
				$"Проверка документа \"{Path.GetFileName(DocumentControl.CurrentDocument.Path)}\" завершена");
			ApplicationLog.LogState("Завершено");
			return resultList;
		}

		/// <summary>
		///    Сериализует текущие параметры проверок в json.
		/// </summary>
		/// <param name="toStream"></param>
		public static void Serialize(Stream toStream)
		{
			try
			{
				var serializer = new DataContractJsonSerializer(typeof(IEnumerable<Check>), GetKnownTypes());
				serializer.WriteObject(toStream, AllChecks);
			}
			catch (Exception ex)
			{
				ApplicationLog.LogError(
					$"При сериализации шаблона произошла ошибка типа {ex.GetType()}:{Environment.NewLine}\"{ex.Message}\"");
			}
		}

		/// <summary>
		///    Восстанавливает список проверок и значения их параметров из .json.
		/// </summary>
		/// <param name="fromStream"></param>
		public static void Deserialize(Stream fromStream)
		{
			try
			{
				var serializer = new DataContractJsonSerializer(typeof(IEnumerable<Check>), GetKnownTypes());
				var fakeChecks = (IEnumerable<Check>) serializer.ReadObject(fromStream);

				Clear();

				foreach (var fakeCheck in fakeChecks)
					Add(fakeCheck.GetType()).CopyValuesFromDeserialized(fakeCheck);
			}
			catch (Exception ex) //Ошибки при десериализации параметров ловятся и логируются внутри
			{
				Clear();
				ApplicationLog.LogError($"При десериализации шаблона произошла ошибка типа {ex.GetType()}:"
												+ Environment.NewLine + $"\"{ex.Message}\""
												+ Environment.NewLine + "Список проверок очищен.");
			}
		}

		private static IEnumerable<Type> GetKnownTypes()
		{
			var assembly = Assembly.GetAssembly(typeof(Check));
			return assembly.GetTypes()
				.Where(type =>
					(type.IsSubclassOf(typeof(Check)) || type.IsSubclassOf(typeof(CheckParameter)))
					&& !type.ContainsGenericParameters && !type.IsAbstract)
				.ToList();
		}

		public static IEnumerable<Type> GetAllChecksTypes()
		{
			var assembly = Assembly.GetAssembly(typeof(Check));
			return assembly.GetTypes()
				.Where(type =>
					type.IsSubclassOf(typeof(Check)) && !type.IsAbstract)
				.ToList();
		}
	}

	public class CheckListEventArgs : EventArgs
	{
		public CheckListEventArgs(Check check)
		{
			Check = check;
		}

		public readonly Check Check;
	}

	public delegate void CheckListEventHandler(CheckListEventArgs e);
}