﻿using System;
using System.IO;

namespace DocChecker.Core
{
	public static class TemplatesOperations
	{
		public static void Export(String path)
		{
			var stream = new FileStream(path, FileMode.Create, FileAccess.Write);
			CheckList.Serialize(stream);
			stream.Dispose();
		}

		public static void Load(String path)
		{
			var stream = new FileStream(path, FileMode.Open, FileAccess.Read);
			CheckList.Deserialize(stream);
			stream.Dispose();
		}
	}
}