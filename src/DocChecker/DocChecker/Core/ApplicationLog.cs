﻿using System;
using System.Windows.Forms;
using DocChecker.Properties;

namespace DocChecker.Core
{
	/// <summary>
	///    Простой класс для записи ошибок и предупреждений.
	/// </summary>
	public static class ApplicationLog
	{
		public static event Action<String> LogCalled = message => { };
		public static event Action<String> LogStateCalled = message => { };
		public static event Action<TreeNode> LogTreeCalled = message => { };

		public static void LogState(String text)
		{
			LogChangeState(text);
		}

		public static void LogMessage(String message)
		{
			LogText("Message: " + message);
		}

		public static void LogWarning(String message)
		{
			LogText("Warning: " + message);
		}

		public static void LogError(String message)
		{
			LogText("Error: " + message);
		}

		public static void LogResult(CheckResult checkResult)
		{
			String message =
				$"Check: {checkResult.Check.Name}: {(checkResult.Success ? "успех." : "несоответствие.")}";
			var checkNode =
				new TreeNode(
					$"{checkResult.Check.Name}: {(checkResult.Success ? "успех." : "несоответствие.")}");
			if (!checkResult.Success)
			{
				message += $"{Environment.NewLine}  {checkResult.Message}";
				if (checkResult.Occurrences != null)
				{
					message += Environment.NewLine + "  Вхождения: " + Environment.NewLine;
					foreach (var parameter in checkResult.Occurrences)
					{
						message += "   " + parameter.Key + Environment.NewLine;
						var parameterNode = new TreeNode(parameter.Key);
						Int32 previousPage = 0;
						TreeNode pageNode = null;
						if (parameter.Value != null)
						{
							foreach (var fail in parameter.Value)
							{
								message += "     ";
								if (previousPage != InteropHelper.GetPageNumberOfRange(fail.Range))
								{
									previousPage = InteropHelper.GetPageNumberOfRange(fail.Range);
									if (pageNode != null) parameterNode.Nodes.Add(pageNode);
									pageNode = new TreeNode($"Cтраница {previousPage}");
								}
								if (Settings.Default.ShowLineNumbers)
								{
									message += $"стр. {InteropHelper.GetPageNumberOfRange(fail.Range)}";
									if (Settings.Default.ShowShortifiedParagraph) message += ": ";
								}
								if (Settings.Default.ShowShortifiedParagraph) message += $"\"{InteropHelper.GetShortedParagraph(fail)}...\"";
								message += Environment.NewLine;
								pageNode.Nodes.Add($"\"{InteropHelper.GetShortedParagraph(fail)}...\"");
							}
						}
						if (!parameterNode.Nodes.Contains(pageNode)) parameterNode.Nodes.Add(pageNode);
						checkNode.Nodes.Add(parameterNode);
					}
				}
			}
			LogTreeNode(checkNode);
			LogText(message);
		}

		private static void LogTreeNode(TreeNode rootNode)
		{
			LogTreeCalled.Invoke(rootNode);
		}

		private static void LogText(String text)
		{
			LogCalled.Invoke(text);
		}

		private static void LogChangeState(String text)
		{
			LogStateCalled.Invoke(text);
		}
	}
}