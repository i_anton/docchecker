﻿using System;
using System.Runtime.Serialization;

namespace DocChecker.Core.Checks.Parameters
{
	/// <summary>
	///    Представляет базовый класс параметров проверок.
	/// </summary>
	[DataContract]
	public abstract class CheckParameter
	{
		protected CheckParameter(String name, CheckParameter enabledDependenceParameter = null)
		{
			Name = name;
			EnabledDependenceParameter = enabledDependenceParameter;

			if (EnabledDependenceParameter != null)
				EnabledDependenceParameter.EnabledChanged += OnEnableDependenceParameterEnabledChanged;
			//TODO: Нет отписки, придумать когда и добавить.
		}

		/// <summary>
		///    Будет ли проверяться данный параметр.
		/// </summary>
		[DataMember]
		public Boolean Enabled
		{
			get { return _enabled; }
			set
			{
				if (_enabled == value) return;

				_enabled = value && (EnabledDependenceParameter?.Enabled ?? true);
				EnabledChanged?.Invoke(this, EventArgs.Empty);
			}
		}

		/// <summary>
		///    Данный параметр будет недоступен для включения (и будет гарантированно выключен), если указанный - выключен.
		/// </summary>
		public CheckParameter EnabledDependenceParameter { get; }

		/// <summary>
		///    Название параметра.
		/// </summary>
		public String Name { get; }

		private void OnEnableDependenceParameterEnabledChanged(Object sender, EventArgs e)
		{
			if (!EnabledDependenceParameter.Enabled)
				Enabled = false;
		}

		/// <summary>
		///    Вызывается при изменении значения параметра.
		///    Изменение значения на тоже самое не повлечет вызов этого события.
		/// </summary>
		public event EventHandler ValueChanged;

		/// <summary>
		///    Вызывается при изменении состояния параметра (вкл/выкл).
		/// </summary>
		/// <remarks>
		///    Может также быть вызвано без реального изменения состояния вследствие "блокировки" со стороны
		///    EnabledDependenceParameter.
		/// </remarks>
		public event EventHandler EnabledChanged;

		protected abstract void CopyValuesFromDeserialized(CheckParameter fakeParameter);

		public void SafeCopyValuesFromDeserialized(CheckParameter fakeParameter)
		{
			try
			{
				CopyValuesFromDeserialized(fakeParameter);
			}
			catch (NullReferenceException)
			{
				ApplicationLog.LogWarning(
					$"Ошибка при загрузке шаблона: параметр типа {GetType()}: \"{Name}\" не удалось восстановить: данный параметр в шаблоне отсутствует."
					+ Environment.NewLine + "Будет использовано значение по умолчанию.");
			}
			catch (ArgumentOutOfRangeException)
			{
				ApplicationLog.LogWarning(
					$"Ошибка при загрузке шаблона: параметр типа {GetType()}: \"{Name}\" не удалось восстановить: в шаблоне указано недопустимое значение."
					+ Environment.NewLine + "Будет использовано значение по умолчанию.");
			}
			catch (Exception ex)
			{
				ApplicationLog.LogWarning(
					$"Ошибка при загрузке шаблона: параметр типа {GetType()}: \"{Name}\" не удалось восстановить: неизвестная ошибка типа {ex.GetType()}: {ex.Message}"
					+ Environment.NewLine + "Будет использовано значение по умолчанию.");
			}
		}

		protected virtual void OnValueChanged()
		{
			ValueChanged?.Invoke(this, EventArgs.Empty);
		}

		private Boolean _enabled;
	}
}