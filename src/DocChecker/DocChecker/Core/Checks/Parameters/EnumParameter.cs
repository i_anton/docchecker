﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace DocChecker.Core.Checks.Parameters
{
	/// <summary>
	///    Параметр перечисляемого типа.
	/// </summary>
	/// <typeparam name="T">Должен быть перечисляемого типа.</typeparam>
	[DataContract]
	public class EnumParameter<T> : CheckParameter
	{
		public EnumParameter(String name, CheckParameter enabledDependenceParameter = null)
			: base(name, enabledDependenceParameter)
		{
		}

		// ReSharper disable once StaticMemberInGenericType будьте осторожны, не меняйте его в рантайме.
		private static readonly Dictionary<String, String> EnumToRusDictionary = new Dictionary<String, String>
		{
			{"wdPaper10x14", "10x14"}, //Value должны быть уникальными, защиты от дуркак не стоит
			{"wdPaper11x17", "11x17"},
			{"wdPaperLetter", "Letter"},
			{"wdPaperLetterSmall", "Small letter"},
			//{"wdPaperLegal", "Legal"},
			//{"wdPaperExecutive", "Executive"},
			{"wdPaperA3", "A3"},
			{"wdPaperA4", "A4"},
			{"wdPaperA4Small", "Малый A4"},
			{"wdPaperA5", "A5"},
			{"wdPaperB4", "B4"},
			{"wdPaperB5", "B5"},
			//{"wdPaperCSheet", "CSheet"},
			//{"wdPaperDSheet", "DSheet"},
			//{"wdPaperESheet", "ESheet"},
			//{"wdPaperFanfoldLegalGerman", "Legal German fanfold"},
			//{"wdPaperFanfoldStdGerman", "Standart German fanfold"},
			//{"wdPaperFanfoldUS", "US fanfold"},
			//{"wdPaperFolio", "Фолио"},
			{"wdPaperLedger", "Ledger"},
			//{"wdPaperNote", "Note"},
			//{"wdPaperQuarto", "Quarto"},
			//{"wdPaperStatement", "Statement"},
			//{"wdPaperTabloid", "Таблоид"},
			//{"wdPaperEnvelope9", "Envelope 9"},
			//{"wdPaperEnvelope10", "Envelope 10"},
			//{"wdPaperEnvelope11", "Envelope 11"},
			//{"wdPaperEnvelope12", "Envelope 12"},
			//{"wdPaperEnvelope14", "Envelope 14"},
			//{"wdPaperEnvelopeB4", "Envelope B4"},
			//{"wdPaperEnvelopeB5", "Envelope B5"},
			//{"wdPaperEnvelopeB6", "Envelope B6"},
			//{"wdPaperEnvelopeC3", "Envelope C3"},
			//{"wdPaperEnvelopeC4", "Envelope C4"},
			//{"wdPaperEnvelopeC5", "Envelope C5"},
			//{"wdPaperEnvelopeC6", "Envelope C6"},
			//{"wdPaperEnvelopeC65", "Envelope C65"},
			//{"wdPaperEnvelopeDL", "Envelope DL"},
			//{"wdPaperEnvelopeItaly", "Envelope Italy"},
			//{"wdPaperEnvelopeMonarch", "Envelope monarch"},
			//{"wdPaperEnvelopePersonal", "Envelope personal"},
			//{"wdPaperCustom", ""},
			{"wdOrientPortrait", "Портретная"},
			{"wdOrientLandscape", "Альбомная"},
			{"wdLineSpaceSingle", "Одинарный"},
			{"wdLineSpace1pt5", "1,5 строки"},
			{"wdLineSpaceDouble", "Двойной"},
			//{"wdLineSpaceAtLeast", "Space at least"},
			//{"wdLineSpaceExactly", "Space exactly"},
			//{"wdLineSpaceMultiple", "Space multiple"},
			{"wdAlignParagraphLeft", "По левому краю"},
			{"wdAlignParagraphCenter", "По центру"},
			{"wdAlignParagraphRight", "По правому краю"},
			{"wdAlignParagraphJustify", "По ширине"},
			//{"wdAlignParagraphDistribute", ""},
			//{"wdAlignParagraphJustifyMed", ""},
			//{"wdAlignParagraphJustifyHi", ""},
			//{"wdAlignParagraphJustifyLow", ""},
			//{"wdAlignParagraphThaiJustify", ""},
			{"wdPageNumberStyleArabic", "1, 2, 3, ..."},
			{"wdPageNumberStyleUppercaseRoman", "I, II, III, ..."},
			{"wdPageNumberStyleLowercaseRoman", "i, ii, iii, ..."},
			{"wdPageNumberStyleUppercaseLetter", "A, B, C, ..."},
			{"wdPageNumberStyleLowercaseLetter", "a, b, c, ..."},
			//{"wdPageNumberStyleKanji", ""},
			//{"wdPageNumberStyleKanjiDigit", ""},
			//{"wdPageNumberStyleArabicFullWidth", ""},
			//{"wdPageNumberStyleKanjiTraditional", ""},
			//{"wdPageNumberStyleNumberInCircle", ""},
			//{"wdPageNumberStyleTradChinNum1", ""},
			//{"wdPageNumberStyleTradChinNum2", ""},
			//{"wdPageNumberStyleSimpChinNum1", ""},
			//{"wdPageNumberStyleSimpChinNum2", ""},
			//{"wdPageNumberStyleHanjaRead", ""},
			//{"wdPageNumberStyleHanjaReadDigit", ""},
			//{"wdPageNumberStyleHebrewLetter1", ""},
			//{"wdPageNumberStyleArabicLetter1", ""},
			//{"wdPageNumberStyleHebrewLetter2", ""},
			//{"wdPageNumberStyleArabicLetter2", ""},
			//{"wdPageNumberStyleHindiLetter1", ""},
			//{"wdPageNumberStyleHindiLetter2", ""},
			//{"wdPageNumberStyleHindiArabic", ""},
			//{"wdPageNumberStyleHindiCardinalText", ""},
			//{"wdPageNumberStyleThaiLetter", ""},
			//{"wdPageNumberStyleThaiArabic", ""},
			//{"wdPageNumberStyleThaiCardinalText", ""},
			//{"wdPageNumberStyleVietCardinalText", ""},
			{"wdPageNumberStyleNumberInDash", " -1-, -2- , -3-, ..."},
			{"wdAlignPageNumberLeft", "Слева"},
			{"wdAlignPageNumberCenter", "Центр"},
			{"wdAlignPageNumberRight", "Справа"},
			//{"wdAlignPageNumberInside", "Внутри"},
			//{"wdAlignPageNumberOutside", "Снаружи"},
			{"wdListNumberStyleArabic", "1, 2, 3"},
			{"wdListNumberStyleUppercaseRoman", "I, II, III"},
			{"wdListNumberStyleLowercaseRoman", "i, ii, iii"},
			{"wdListNumberStyleUppercaseLetter", "A, B, C"},
			{"wdListNumberStyleLowercaseLetter", "a, b, c"},
			{"wdListNumberStyleLowercaseRussian", "а, б, в"},
			{"wdListNumberStyleUppercaseRussian", "A, Б, В"},
			{"None", " "},
			{"Dash", "—"},
			{"Bullet", "●"},
			{"Square", "■"},
			{"Asterisk", "❖"}
		};

		[DataMember]
		public T Value
		{
			get { return _value; }
			set
			{
				if (value.Equals(_value)) return;

				_value = value;
				OnValueChanged();
			}
		}

		public static String SimplifyEnumName(String enumName)
		{
			String value;

			if (EnumToRusDictionary.TryGetValue(enumName, out value))
				return value;
			throw new ArgumentException("Dictionary does not contains specified enum name.");
		}

		public static String GetEnumNameFromSimplifiedName(String knownName)
		{
			foreach (var pair in EnumToRusDictionary)
				if (pair.Value == knownName)
					return pair.Key;
			throw new ArgumentException("Dictionary does not contains specified enum simplified name.");
		}

		protected override void CopyValuesFromDeserialized(CheckParameter fakeParameter)
		{
			var fakeParameterCasted = (EnumParameter<T>) fakeParameter;
			Value = fakeParameterCasted.Value;
			Enabled = fakeParameterCasted.Enabled;
		}

		private T _value;
	}
}