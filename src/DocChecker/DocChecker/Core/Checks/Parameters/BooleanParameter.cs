﻿using System;
using System.Runtime.Serialization;

namespace DocChecker.Core.Checks.Parameters
{
	/// <summary>
	///    Представляет параметр логического типа.
	/// </summary>
	[DataContract]
	public class BooleanParameter : CheckParameter
	{
		public BooleanParameter(String name, CheckParameter enabledDependenceParameter = null)
			: base(name, enabledDependenceParameter)
		{
			EnabledChanged += (sender, e) => OnValueChanged();
		}

		protected override void CopyValuesFromDeserialized(CheckParameter fakeParameter)
		{
			var fakeParameterCasted = (BooleanParameter) fakeParameter;
			Enabled = fakeParameterCasted.Enabled;
		}
	}
}