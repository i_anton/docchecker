﻿using System;
using System.Drawing;
using System.Runtime.Serialization;

namespace DocChecker.Core.Checks.Parameters
{
	/// <summary>
	///    Представляет параметр типа System.Drawing.Font (шрифт).
	/// </summary>
	[DataContract]
	public class FontParameter : CheckParameter
	{
		public FontParameter(String name, CheckParameter enabledDependenceParameter = null)
			: base(name, enabledDependenceParameter)
		{
			_value = new Font("Times New Roman", 12);
		}

		public Font Value
		{
			get { return _value; }
			set
			{
				if (value.Equals(_value)) return;

				_value = value;
				OnValueChanged();
			}
		}

		[DataMember]
		public FontData SerializableValue
		{
			get { return new FontData(Value.FontFamily.Name, Value.Size, Value.Style); }
			set { Value = new Font(value.FontFamilyName, value.Size, value.Style); }
		}

		protected override void CopyValuesFromDeserialized(CheckParameter fakeParameter)
		{
			var fakeParameterCasted = (FontParameter) fakeParameter;
			Value = fakeParameterCasted.Value;
			Enabled = fakeParameterCasted.Enabled;
		}

		private Font _value;
	}

	[DataContract]
	public struct FontData
	{
		public FontData(String fontFamilyName, Single size, FontStyle style)
		{
			FontFamilyName = fontFamilyName;
			Size = size;
			Style = style;
		}

		[DataMember] public readonly String FontFamilyName;
		[DataMember] public readonly Single Size;
		[DataMember] public readonly FontStyle Style;
	}
}