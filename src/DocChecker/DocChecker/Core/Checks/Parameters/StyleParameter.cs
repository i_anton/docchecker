﻿using System;
using System.Runtime.Serialization;
using Microsoft.Office.Interop.Word;

namespace DocChecker.Core.Checks.Parameters
{
	/// <summary>
	///    Представляет параметр, оборачивающий Interop.Word.Style.
	/// </summary>
	[DataContract]
	public class StyleParameter : CheckParameter
	{
		public StyleParameter(String name, CheckParameter enabledDependenceParameter = null)
			: base(name, enabledDependenceParameter)
		{
			_stringValue = String.Empty;
		}

		public Style Value
		{
			get { return _value; }
			set
			{
				_value = value;
				OnValueChanged();
			}
		}

		public String StringValue
		{
			get { return _stringValue; }
			set
			{
				//Проблема: язык. На английской локали будут английские, на русской - русские.
				Style style = null;
				if (DocumentControl.IsOpened)
				{
					var stylesCache = DocumentControl.CurrentDocument.GetInteropDocumentContainer().StylesCache;
					if (stylesCache.CacheContains(value))
						style = stylesCache[value];
				}

				_stringValue = value;
				Value = style;

				if (style == null)
					throw new StyleNotFoundException(
						"Style of this name wasn't found, setting \"Value\" to null and \"StringValue\" to \"value\".");
			}
		}

		protected override void CopyValuesFromDeserialized(CheckParameter fakeParameter)
		{
			ApplicationLog.LogError(StringValue);
			var fakeParameterCasted = (StyleParameter) fakeParameter;

			try
			{
				StringValue = fakeParameterCasted.StringValue;
			}
			catch (StyleNotFoundException)
			{
				//Действия не требуются.
			}

			Enabled = fakeParameterCasted.Enabled;
		}

		private Style _value;

		[DataMember] private String _stringValue;
	}
}