﻿using System;
using System.Runtime.Serialization;

namespace DocChecker.Core.Checks.Parameters
{
	/// <summary>
	///    Представляет параметр числового типа с плавающей запятой.
	/// </summary>
	[DataContract]
	public class FloatParameter : CheckParameter
	{
		public FloatParameter(String name, Single minValue, Single maxValue, CheckParameter enabledDependenceParameter = null)
			: base(name, enabledDependenceParameter)
		{
			MinValue = minValue;
			MaxValue = maxValue;
		}

		public Single Value
		{
			get { return _value; }
			set
			{
				if (value == _value) return;

				if (value >= MinValue && value <= MaxValue)
				{
					_value = value;
					OnValueChanged();
				}
				else
					throw new ArgumentOutOfRangeException(
						$"Value should be greater or equal to {MinValue} and less or equal to {MaxValue}"
						);
			}
		}

		public Single MinValue { get; }
		public Single MaxValue { get; }

		protected override void CopyValuesFromDeserialized(CheckParameter fakeParameter)
		{
			var fakeParameterCasted = (FloatParameter) fakeParameter;
			Value = fakeParameterCasted.Value;
			Enabled = fakeParameterCasted.Enabled;
		}

		[DataMember(Name = "Value")] private Single _value;
	}
}