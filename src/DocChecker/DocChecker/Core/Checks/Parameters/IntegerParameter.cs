﻿using System;
using System.Runtime.Serialization;

namespace DocChecker.Core.Checks.Parameters
{
	/// <summary>
	///    Представляет параметр целочисленного типа.
	/// </summary>
	[DataContract]
	public class IntegerParameter : CheckParameter
	{
		public IntegerParameter(String name, Int32 minValue, Int32 maxValue, CheckParameter enabledDependenceParameter = null)
			: base(name, enabledDependenceParameter)
		{
			MinValue = minValue;
			MaxValue = maxValue;
		}

		public Int32 Value
		{
			get { return _value; }
			set
			{
				if (value == _value) return;

				if (value >= MinValue && value <= MaxValue)
				{
					_value = value;
					OnValueChanged();
				}
				else
					throw new ArgumentOutOfRangeException(
						$"Value should be greater or equal to {MinValue} and less or equal to {MaxValue}"
						);
			}
		}

		public Int32 MinValue { get; }
		public Int32 MaxValue { get; }

		protected override void CopyValuesFromDeserialized(CheckParameter fakeParameter)
		{
			var fakeParameterCasted = (IntegerParameter) fakeParameter;
			Value = fakeParameterCasted.Value;
			Enabled = fakeParameterCasted.Enabled;
		}

		[DataMember(Name = "Value")] private Int32 _value;
	}
}