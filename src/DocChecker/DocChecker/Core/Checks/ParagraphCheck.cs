﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using DocChecker.Core.Checks.Parameters;
using DocChecker.Properties;
using Microsoft.Office.Interop.Word;
using static DocChecker.Core.InteropHelper;

namespace DocChecker.Core.Checks
{
	[DataContract]
	public class ParagraphCheck : Check
	{
		public enum WdListBulletStyle
		{
			None,
			Bullet,
			Dash,
			Asterisk,
			Square
		}

		public ParagraphCheck()
		{
			Spacing = new EnumParameter<WdLineSpacing>("Междустрочный интервал");
			Indent = new FloatParameter("Отступ первой строки", 0, 100);
			Align = new EnumParameter<WdParagraphAlignment>("Выравнивание");
			HangingLines = new BooleanParameter("Запрет висячих строк");
			NumberedListLevel1 = new EnumParameter<WdListNumberStyle>("Нумерованный список 1-го уровня");
			NumberedListLevel2 = new EnumParameter<WdListNumberStyle>("Нумерованный список 2-го уровня");
			BulletedList = new EnumParameter<WdListBulletStyle>("Маркированный список");
			_parameters = new List<CheckParameter>
			{
				Spacing,
				Indent,
				Align,
				HangingLines,
				NumberedListLevel1,
				NumberedListLevel2,
				BulletedList
			};
		}

		[DataMember] public readonly EnumParameter<WdParagraphAlignment> Align;
		[DataMember] public readonly EnumParameter<WdListBulletStyle> BulletedList;
		[DataMember] public readonly BooleanParameter HangingLines;
		[DataMember] public readonly FloatParameter Indent;
		[DataMember] public readonly EnumParameter<WdListNumberStyle> NumberedListLevel1;
		[DataMember] public readonly EnumParameter<WdListNumberStyle> NumberedListLevel2;
		[DataMember] public readonly EnumParameter<WdLineSpacing> Spacing;

		public override String Name => "Абзацы";

		public override CheckResult Invoke(WordDocument wordDocument, ref BackgroundWorker backgroundWorker)
		{
			var document = wordDocument.GetInteropDocument();
			Boolean alignSuccess = true;
			Boolean spacingSuccess = true;
			Boolean indentSuccess = true;
			Boolean hangingSuccess = true;
			Boolean numList1Success = true;
			Boolean numList2Success = true;
			Boolean bulListSuccess = true;
			String bullet = String.Empty;
			var occurrences = new List<KeyValuePair<String, List<Paragraph>>>();
			var alignFail = new KeyValuePair<String, List<Paragraph>>(Settings.Default.ParagraphAlign, new List<Paragraph>());
			var hangingLinesFail = new KeyValuePair<String, List<Paragraph>>(Settings.Default.ParagraphHangingLines,
				new List<Paragraph>());
			var indentFail = new KeyValuePair<String, List<Paragraph>>(Settings.Default.ParagraphIndent, new List<Paragraph>());
			var spacingFail = new KeyValuePair<String, List<Paragraph>>(Settings.Default.ParagraphSpacing, new List<Paragraph>());
			var numList1Fail = new KeyValuePair<String, List<Paragraph>>(Settings.Default.NumberedListLevel1,
				new List<Paragraph>());
			var numList2Fail = new KeyValuePair<String, List<Paragraph>>(Settings.Default.NumberedListLevel2,
				new List<Paragraph>());
			var bulListFail = new KeyValuePair<String, List<Paragraph>>(Settings.Default.BulletedList, new List<Paragraph>());
			if (Spacing.Enabled || BulletedList.Enabled || NumberedListLevel1.Enabled || NumberedListLevel2.Enabled
				 || Indent.Enabled || Align.Enabled || HangingLines.Enabled)
			{
				foreach (Paragraph p in document.Content.Paragraphs)
				{
					if (backgroundWorker.CancellationPending) break;
					var listFormat = p.Range.ListFormat;
					if (listFormat.ListType == WdListType.wdListSimpleNumbering)
					{
						if (NumberedListLevel1.Enabled && (listFormat.ListTemplate.ListLevels[1].NumberStyle != NumberedListLevel1.Value
																	  || listFormat.ListTemplate.ListLevels[1].NumberFormat != "%1."))
						{
							if (numList1Success) numList1Success = false;
							numList1Fail.Value.Add(p);
						}
						if (NumberedListLevel2.Enabled && (listFormat.ListTemplate.ListLevels[2].NumberStyle != NumberedListLevel2.Value
																	  || listFormat.ListTemplate.ListLevels[2].NumberFormat != "%1."))
						{
							if (numList2Success) numList2Success = false;
							numList2Fail.Value.Add(p);
						}
					}
					if (BulletedList.Enabled && listFormat.ListType == WdListType.wdListBullet)
					{
						bullet = String.Empty;
						switch (BulletedList.Value)
						{
							case WdListBulletStyle.Asterisk:
								bullet = "";
								break;
							case WdListBulletStyle.Bullet:
								bullet = "";
								break;
							case WdListBulletStyle.Dash:
								bullet = "";
								break;
							case WdListBulletStyle.Square:
								bullet = "";
								break;
						}
						if (p.Range.ListFormat.ListString != bullet)
						{
							if (bulListSuccess) bulListSuccess = false;
							bulListFail.Value.Add(p);
						}
					}
					if (Spacing.Enabled && Spacing.Value != p.LineSpacingRule)
					{
						if (spacingSuccess) spacingSuccess = false;
						spacingFail.Value.Add(p);
					}
					if (Indent.Enabled && Math.Round(Cm2Pts(Indent.Value)) != Math.Round(p.FirstLineIndent))
					{
						if (indentSuccess) indentSuccess = false;
						indentFail.Value.Add(p);
					}
					if (Align.Enabled && Align.Value != p.Alignment)
					{
						if (alignSuccess) alignSuccess = false;
						alignFail.Value.Add(p);
					}
					if (HangingLines.Enabled && p.WidowControl != -1)
					{
						if (hangingSuccess) hangingSuccess = false;
						hangingLinesFail.Value.Add(p);
					}
				}
			}
			if (spacingFail.Value.Count != 0) occurrences.Add(spacingFail);
			if (indentFail.Value.Count != 0) occurrences.Add(indentFail);
			if (alignFail.Value.Count != 0) occurrences.Add(alignFail);
			if (hangingLinesFail.Value.Count != 0) occurrences.Add(hangingLinesFail);
			if (numList1Fail.Value.Count != 0) occurrences.Add(numList1Fail);
			if (numList2Fail.Value.Count != 0) occurrences.Add(numList2Fail);
			if (bulListFail.Value.Count != 0) occurrences.Add(bulListFail);
			Boolean success = alignSuccess && indentSuccess && spacingSuccess && hangingSuccess && numList1Success &&
									numList2Success && bulListSuccess;

			return new CheckResult(success, String.Empty, this, occurrences.Count != 0 ? occurrences : null);
		}

		public override IEnumerable<CheckParameter> GetParameters()
		{
			return _parameters;
		}

		public override void CopyValuesFromDeserialized(Check fakeCheck)
		{
			var fakeCheckCasted = (ParagraphCheck) fakeCheck;

			Spacing.SafeCopyValuesFromDeserialized(fakeCheckCasted.Spacing);
			Align.SafeCopyValuesFromDeserialized(fakeCheckCasted.Align);
			Indent.SafeCopyValuesFromDeserialized(fakeCheckCasted.Indent);
			HangingLines.SafeCopyValuesFromDeserialized(fakeCheckCasted.HangingLines);
			NumberedListLevel1.SafeCopyValuesFromDeserialized(fakeCheckCasted.NumberedListLevel1);
			NumberedListLevel2.SafeCopyValuesFromDeserialized(fakeCheckCasted.NumberedListLevel2);
			BulletedList.SafeCopyValuesFromDeserialized(fakeCheckCasted.BulletedList);
		}

		private readonly IEnumerable<CheckParameter> _parameters;
	}
}