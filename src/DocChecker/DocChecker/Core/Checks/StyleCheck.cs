﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using DocChecker.Core.Checks.Parameters;
using DocChecker.Properties;
using Microsoft.Office.Interop.Word;
using static DocChecker.Core.InteropHelper;

namespace DocChecker.Core.Checks
{
	[DataContract]
	internal class StyleCheck : Check
	{
		public StyleCheck()
		{
			Style = new StyleParameter("Стиль");
			Font = new FontParameter("Шрифт",Style);
			FontSize = new BooleanParameter("Проверка размера шрифта",Font);
			FontBlack = new BooleanParameter("Только чёрный текст",Font);
			FontNotStylized = new BooleanParameter("Без стилей текста",Font);
			Spacing = new EnumParameter<WdLineSpacing>("Междустрочный интервал",Style);
			Indent = new FloatParameter("Отступ первой строки", 0, 100,Style);
			Align = new EnumParameter<WdParagraphAlignment>("Выравнивание",Style);
			HangingLines = new BooleanParameter("Запрет висячих строк",Style);

			Style.ValueChanged += (sender, e) => OnNameChanged();

			_parameters = new List<CheckParameter> {Style, Font, FontSize, FontBlack, FontNotStylized, Align, Spacing, Indent, HangingLines};
		}

		[DataMember] public readonly FontParameter Font;
		[DataMember] public readonly BooleanParameter FontBlack;
		[DataMember] public readonly BooleanParameter FontNotStylized;
		[DataMember] public readonly BooleanParameter FontSize;
		[DataMember] public readonly StyleParameter Style;
		[DataMember] public readonly EnumParameter<WdParagraphAlignment> Align;
		[DataMember] public readonly BooleanParameter HangingLines;
		[DataMember] public readonly FloatParameter Indent;
		[DataMember] public readonly EnumParameter<WdLineSpacing> Spacing;

		public override String Name => $"Стиль ({Style.StringValue})";

		public override CheckResult Invoke(WordDocument wordDocument, ref BackgroundWorker backgroundWorker)
		{
			var document = wordDocument.GetInteropDocument();
			Boolean fontSuccess = true;
			Boolean fontSizeSuccess = true;
			Boolean fontBlackSuccess = true;
			Boolean fontNotStylizedSuccess = true;
			Boolean alignSuccess = true;
			Boolean spacingSuccess = true;
			Boolean indentSuccess = true;
			Boolean hangingSuccess = true;
			var occurrences = new List<KeyValuePair<String, List<Paragraph>>>();
			var fontFail = new KeyValuePair<String, List<Paragraph>>(Settings.Default.FontFont, new List<Paragraph>());
			var fontBlackFail = new KeyValuePair<String, List<Paragraph>>(Settings.Default.FontBlack, new List<Paragraph>());
			var fontNotStylizedFail = new KeyValuePair<String, List<Paragraph>>(Settings.Default.FontNotStylized,
				new List<Paragraph>());
			var fontSizeFail = new KeyValuePair<String, List<Paragraph>>(Settings.Default.FontSize, new List<Paragraph>());
			var alignFail = new KeyValuePair<String, List<Paragraph>>(Settings.Default.ParagraphAlign, new List<Paragraph>());
			var hangingLinesFail = new KeyValuePair<String, List<Paragraph>>(Settings.Default.ParagraphHangingLines,
				new List<Paragraph>());
			var indentFail = new KeyValuePair<String, List<Paragraph>>(Settings.Default.ParagraphIndent, new List<Paragraph>());
			var spacingFail = new KeyValuePair<String, List<Paragraph>>(Settings.Default.ParagraphSpacing, new List<Paragraph>());

			if (Style.Enabled)
			{
				foreach (Paragraph p in document.Content.Paragraphs)
				{
					if (backgroundWorker.CancellationPending) break;
					Style pStyle = p.get_Style();
					if (pStyle.NameLocal != Style.StringValue) continue;
					if (Font.Enabled)
					{
						if (Font.Value.Name != p.Range.Font.Name)
						{
							if (fontSuccess) fontSuccess = false;
							fontFail.Value.Add(p);
						}
						if (FontSize.Enabled && Math.Round(Font.Value.Size) != Math.Round(p.Range.Font.Size))
						{
							if (fontSizeSuccess) fontSizeSuccess = false;
							fontSizeFail.Value.Add(p);
						}
					}
					if (FontBlack.Enabled && p.Range.Font.Color != WdColor.wdColorAutomatic &&
						 p.Range.Font.Color != WdColor.wdColorBlack)
					{
						if (fontBlackSuccess) fontBlackSuccess = false;
						fontBlackFail.Value.Add(p);
					}
					if (FontNotStylized.Enabled &&
						 (p.Range.Font.Bold != 0 || p.Range.Italic != 0 || p.Range.Underline != WdUnderline.wdUnderlineNone ||
						  p.Range.Font.Subscript != 0 || p.Range.Font.StrikeThrough != 0 || p.Range.Font.Superscript != 0 ||
						  p.Range.Font.Shadow != 0 || p.Range.Font.Hidden != 0 || p.Range.Font.Outline != 0))
					{
						if (fontNotStylizedSuccess) fontNotStylizedSuccess = false;
						fontNotStylizedFail.Value.Add(p);
					}
					if (Spacing.Enabled && Spacing.Value != p.LineSpacingRule)
					{
						if (spacingSuccess) spacingSuccess = false;
						spacingFail.Value.Add(p);
					}
					if (Indent.Enabled && Math.Round(Cm2Pts(Indent.Value)) != Math.Round(p.FirstLineIndent))
					{
						if (indentSuccess) indentSuccess = false;
						indentFail.Value.Add(p);
					}
					if (Align.Enabled && Align.Value != p.Alignment)
					{
						if (alignSuccess) alignSuccess = false;
						alignFail.Value.Add(p);
					}
					if (HangingLines.Enabled && p.WidowControl != -1)
					{
						if (hangingSuccess) hangingSuccess = false;
						hangingLinesFail.Value.Add(p);
					}
				}
			}
			if (fontFail.Value.Count != 0) occurrences.Add(fontFail);
			if (fontSizeFail.Value.Count != 0) occurrences.Add(fontSizeFail);
			if (fontBlackFail.Value.Count != 0) occurrences.Add(fontBlackFail);
			if (spacingFail.Value.Count != 0) occurrences.Add(spacingFail);
			if (indentFail.Value.Count != 0) occurrences.Add(indentFail);
			if (alignFail.Value.Count != 0) occurrences.Add(alignFail);
			if (hangingLinesFail.Value.Count != 0) occurrences.Add(hangingLinesFail);
			if (fontNotStylizedFail.Value.Count != 0) occurrences.Add(fontNotStylizedFail);
			Boolean success = fontSizeSuccess && fontSuccess && fontBlackSuccess && fontNotStylizedSuccess && alignSuccess 
				&& indentSuccess && hangingSuccess && spacingSuccess;
			return new CheckResult(success, String.Empty, this, occurrences.Count != 0 ? occurrences : null);
		}

		public override IEnumerable<CheckParameter> GetParameters()
		{
			return _parameters;
		}

		public override void CopyValuesFromDeserialized(Check fakeCheck)
		{
			var fakeCheckCasted = (StyleCheck) fakeCheck;

			Style.SafeCopyValuesFromDeserialized(fakeCheckCasted.Style);
			Font.SafeCopyValuesFromDeserialized(fakeCheckCasted.Font);
			FontSize.SafeCopyValuesFromDeserialized(fakeCheckCasted.FontSize);
			FontBlack.SafeCopyValuesFromDeserialized(fakeCheckCasted.FontBlack);
			Spacing.SafeCopyValuesFromDeserialized(fakeCheckCasted.Spacing);
			Align.SafeCopyValuesFromDeserialized(fakeCheckCasted.Align);
			Indent.SafeCopyValuesFromDeserialized(fakeCheckCasted.Indent);
			HangingLines.SafeCopyValuesFromDeserialized(fakeCheckCasted.HangingLines);
			FontNotStylized.SafeCopyValuesFromDeserialized(fakeCheckCasted.FontNotStylized);
		}

		private readonly IEnumerable<CheckParameter> _parameters;
	}
}