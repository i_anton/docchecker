﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using DocChecker.Core.Checks.Parameters;
using DocChecker.Properties;
using Microsoft.Office.Interop.Word;

namespace DocChecker.Core.Checks
{
	[DataContract]
	internal class NumerationCheck : Check
	{
		public NumerationCheck()
		{
			IsNumerated = new BooleanParameter("Наличие нумерации");
			IsNotRestarted = new BooleanParameter("Не сбрасывается в разделах", IsNumerated);
			NoNumerationTop = new BooleanParameter("Нет нумерации сверху", IsNumerated);
			NumerationStyle = new EnumParameter<WdPageNumberStyle>("Стиль нумерации", IsNumerated);
			Align = new EnumParameter<WdPageNumberAlignment>("Выравнивание", IsNumerated);
			DifferentFirstPage = new BooleanParameter("Первая страница без номера", IsNumerated);
			DifferentLastPage = new BooleanParameter("Последняя страница без номера", IsNumerated);

			_parameters = new List<CheckParameter>
			{
				IsNumerated,
				IsNotRestarted,
				NoNumerationTop,
				NumerationStyle,
				Align,
				DifferentFirstPage,
				DifferentLastPage
			};
		}

		[DataMember] public readonly EnumParameter<WdPageNumberAlignment> Align;
		[DataMember] public readonly BooleanParameter DifferentFirstPage;
		[DataMember] public readonly BooleanParameter DifferentLastPage;
		[DataMember] public readonly BooleanParameter IsNotRestarted;
		[DataMember] public readonly BooleanParameter IsNumerated;
		[DataMember] public readonly BooleanParameter NoNumerationTop;
		[DataMember] public readonly EnumParameter<WdPageNumberStyle> NumerationStyle;

		public override String Name => "Нумерация страниц";

		public override CheckResult Invoke(WordDocument wordDocument, ref BackgroundWorker backgroundWorker)
		{
			var document = wordDocument.GetInteropDocument();
			var sections = document.Sections;
			Boolean success = true;
			Boolean successNumeration = true;
			Boolean successNumerationTop = true;
			Boolean successNotRestarted = true;
			Boolean successStyle = true;
			Boolean successLast = true;
			Boolean successFirst = true;
			Boolean successAlign = true;
			var occurrences = new List<KeyValuePair<String, List<Paragraph>>>();

			if (IsNumerated.Enabled)
			{
				foreach (Section s in sections)
				{
					if (backgroundWorker.CancellationPending) break;
					foreach (HeaderFooter header in s.Headers)
					{
						if (header.PageNumbers.Count != 0 && successNumerationTop)
						{
							successNumerationTop = false;
							occurrences.Add(new KeyValuePair<String, List<Paragraph>>(Settings.Default.NoNumerationTop, null));
						}
					}
					foreach (HeaderFooter footer in s.Footers)
					{
						if (backgroundWorker.CancellationPending) break;
						if (!footer.Exists) continue;
						if (footer.PageNumbers.Count == 0)
						{
							if (successNumeration)
							{
								successNumeration = false;
								occurrences.Add(new KeyValuePair<String, List<Paragraph>>(Settings.Default.IsNumerated, null));
							}
							continue;
						}
						if (IsNotRestarted.Enabled && footer.PageNumbers.RestartNumberingAtSection && successNotRestarted)
						{
							successNotRestarted = false;
							occurrences.Add(new KeyValuePair<String, List<Paragraph>>(Settings.Default.IsNotRestarted, null));
						}

						if (NumerationStyle.Enabled && footer.PageNumbers.NumberStyle != NumerationStyle.Value && successStyle)
						{
							successStyle = false;
							occurrences.Add(new KeyValuePair<String, List<Paragraph>>(Settings.Default.NumerationStyle, null));
						}

						var alignLikeParagraph = WdParagraphAlignment.wdAlignParagraphCenter;
						switch (Align.Value)
						{
							case WdPageNumberAlignment.wdAlignPageNumberLeft:
								alignLikeParagraph = WdParagraphAlignment.wdAlignParagraphLeft;
								break;
							case WdPageNumberAlignment.wdAlignPageNumberRight:
								alignLikeParagraph = WdParagraphAlignment.wdAlignParagraphRight;
								break;
						}

						if (Align.Enabled && footer.Range.ParagraphFormat.Alignment != alignLikeParagraph && successAlign)
						{
							successAlign = false;
							occurrences.Add(new KeyValuePair<String, List<Paragraph>>(Settings.Default.NumerationAlign, null));
						}
					}
				}

				if (DifferentFirstPage.Enabled && document.PageSetup.DifferentFirstPageHeaderFooter != -1 &&
					 sections.First.Footers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].PageNumbers.Count != 0)
				{
					successFirst = false;
					occurrences.Add(new KeyValuePair<String, List<Paragraph>>(Settings.Default.DifferentFirstPage, null));
				}
				if (DifferentLastPage.Enabled && (!sections.Last.Footers[WdHeaderFooterIndex.wdHeaderFooterPrimary].Exists
															 || !sections.Last.Footers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].Exists))
				{
					successLast = false;
					occurrences.Add(new KeyValuePair<String, List<Paragraph>>(Settings.Default.DifferentLastPage, null));
				}
			}

			success = successNumeration && successStyle && successLast && successFirst & successAlign && successNumerationTop 
				&& successNotRestarted;

			return new CheckResult(success, String.Empty, this, occurrences.Count != 0 ? occurrences : null);
		}

		public override IEnumerable<CheckParameter> GetParameters()
		{
			return _parameters;
		}

		public override void CopyValuesFromDeserialized(Check fakeCheck)
		{
			var fakeCheckCasted = (NumerationCheck) fakeCheck;

			IsNumerated.SafeCopyValuesFromDeserialized(fakeCheckCasted.IsNumerated);
			IsNotRestarted.SafeCopyValuesFromDeserialized(fakeCheckCasted.IsNotRestarted);
			NoNumerationTop.SafeCopyValuesFromDeserialized(fakeCheckCasted.NoNumerationTop);
			NumerationStyle.SafeCopyValuesFromDeserialized(fakeCheckCasted.NumerationStyle);
			Align.SafeCopyValuesFromDeserialized(fakeCheckCasted.Align);
			DifferentFirstPage.SafeCopyValuesFromDeserialized(fakeCheckCasted.DifferentFirstPage);
			DifferentLastPage.SafeCopyValuesFromDeserialized(fakeCheckCasted.DifferentLastPage);
		}

		private readonly IEnumerable<CheckParameter> _parameters;
	}
}