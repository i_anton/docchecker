﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using DocChecker.Core.Checks.Parameters;
using DocChecker.Properties;
using Microsoft.Office.Interop.Word;

namespace DocChecker.Core.Checks
{
	[DataContract]
	internal class FontCheck : Check
	{
		public FontCheck()
		{
			Font = new FontParameter("Шрифт");
			FontSize = new BooleanParameter("Проверка размера шрифта",Font);
			FontBlack = new BooleanParameter("Только чёрный текст",Font);
			FontNotStylized = new BooleanParameter("Без стилей",Font);
			_parameters = new List<CheckParameter> {Font, FontSize, FontBlack, FontNotStylized};
		}

		[DataMember] public readonly FontParameter Font;
		[DataMember] public readonly BooleanParameter FontBlack;
		[DataMember] public readonly BooleanParameter FontNotStylized;
		[DataMember] public readonly BooleanParameter FontSize;

		public override String Name => $"Шрифты (ВСЁ)";

		public override CheckResult Invoke(WordDocument wordDocument, ref BackgroundWorker backgroundWorker)
		{
			var document = wordDocument.GetInteropDocument();
			Boolean fontSuccess = true;
			Boolean fontSizeSuccess = true;
			Boolean fontBlackSuccess = true;
			Boolean fontNotStylizedSuccess = true;
			var occurrences = new List<KeyValuePair<String, List<Paragraph>>>();
			var fontFail = new KeyValuePair<String, List<Paragraph>>(Settings.Default.FontFont, new List<Paragraph>());
			var fontBlackFail = new KeyValuePair<String, List<Paragraph>>(Settings.Default.FontBlack, new List<Paragraph>());
			var fontNotStylizedFail = new KeyValuePair<String, List<Paragraph>>(Settings.Default.FontNotStylized,
				new List<Paragraph>());
			var fontSizeFail = new KeyValuePair<String, List<Paragraph>>(Settings.Default.FontSize, new List<Paragraph>());
			if (Font.Enabled)
			{
				foreach (Paragraph p in document.Content.Paragraphs)
				{
					if (backgroundWorker.CancellationPending) break;
					if (Font.Value.Name != p.Range.Font.Name)
					{
						if (fontSuccess) fontSuccess = false;
						fontFail.Value.Add(p);
					}
					if (FontSize.Enabled && Math.Round(Font.Value.Size) != Math.Round(p.Range.Font.Size))
					{
						if (fontSizeSuccess) fontSizeSuccess = false;
						fontSizeFail.Value.Add(p);
					}
					if (FontBlack.Enabled && p.Range.Font.Color != WdColor.wdColorAutomatic &&
						 p.Range.Font.Color != WdColor.wdColorBlack)
					{
						if (fontBlackSuccess) fontBlackSuccess = false;
						fontBlackFail.Value.Add(p);
					}
					if (FontNotStylized.Enabled &&
						 (p.Range.Font.Bold != 0 || p.Range.Italic != 0 || p.Range.Underline != WdUnderline.wdUnderlineNone ||
						  p.Range.Font.Subscript != 0 || p.Range.Font.StrikeThrough != 0 || p.Range.Font.Superscript != 0 ||
						  p.Range.Font.Shadow != 0 || p.Range.Font.Hidden != 0 || p.Range.Font.Outline != 0))
					{
						if (fontNotStylizedSuccess) fontNotStylizedSuccess = false;
						fontNotStylizedFail.Value.Add(p);
					}
				}
			}
			if (fontFail.Value.Count != 0) occurrences.Add(fontFail);
			if (fontSizeFail.Value.Count != 0) occurrences.Add(fontSizeFail);
			if (fontBlackFail.Value.Count != 0) occurrences.Add(fontBlackFail);
			if (fontNotStylizedFail.Value.Count != 0) occurrences.Add(fontNotStylizedFail);
			Boolean success = fontSizeSuccess && fontSuccess && fontBlackSuccess && fontNotStylizedSuccess;
			return new CheckResult(success, String.Empty, this, occurrences.Count != 0 ? occurrences : null);
		}

		public override IEnumerable<CheckParameter> GetParameters()
		{
			return _parameters;
		}

		public override void CopyValuesFromDeserialized(Check fakeCheck)
		{
			var fakeCheckCasted = (FontCheck) fakeCheck;

			Font.SafeCopyValuesFromDeserialized(fakeCheckCasted.Font);
			FontSize.SafeCopyValuesFromDeserialized(fakeCheckCasted.FontSize);
			FontBlack.SafeCopyValuesFromDeserialized(fakeCheckCasted.FontBlack);
			FontNotStylized.SafeCopyValuesFromDeserialized(fakeCheckCasted.FontNotStylized);
		}

		private readonly IEnumerable<CheckParameter> _parameters;
	}
}