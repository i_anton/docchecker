﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using DocChecker.Core.Checks.Parameters;

namespace DocChecker.Core.Checks
{
	/// <summary>
	///    Предоставляет базовый класс для проверок.
	/// </summary>
	[DataContract]
	public abstract class Check
	{
		/// <summary>
		///    Название проверки.
		/// </summary>
		public abstract String Name { get; }

		/// <summary>
		///    Оповещает об изменении названия проверки.
		/// </summary>
		/// <remarks>
		///    Используется, если проверка может менять свое название в зависимости от состояния.
		/// </remarks>
		public event EventHandler NameChanged;

		/// <summary>
		///    Вызов проверки для указанного документа.
		/// </summary>
		public abstract CheckResult Invoke(WordDocument wordDocument, ref BackgroundWorker backgroundWorker);

		/// <summary>
		///    Получение коллекции параметров.
		/// </summary>
		public abstract IEnumerable<CheckParameter> GetParameters();

		/// <summary>
		///    Копирует значения параметров из объекта Check, созданого DataContractJsonSerializer.
		/// </summary>
		/// <remarks>
		///    Как правило, копировать понадобится только непосредственное значение параметра.
		///    Переданный на вход аргумент должен быть объектом того же типа, что и сам наследник (C# не позволяет указать это
		///    явно).
		/// </remarks>
		public abstract void CopyValuesFromDeserialized(Check fakeCheck);

		/// <summary>
		///    Должен использоваться наследниками для оповещения об изменении названия проверки.
		/// </summary>
		protected void OnNameChanged()
		{
			NameChanged?.Invoke(this, EventArgs.Empty);
		}
	}
}