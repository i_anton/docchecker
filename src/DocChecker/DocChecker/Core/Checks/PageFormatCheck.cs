﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using DocChecker.Core.Checks.Parameters;
using DocChecker.Properties;
using Microsoft.Office.Interop.Word;
using static DocChecker.Core.InteropHelper;

namespace DocChecker.Core.Checks
{
	[DataContract]
	internal class PageFormatCheck : Check
	{
		public PageFormatCheck()
		{
			PaperSize = new EnumParameter<WdPaperSize>("Размер страницы");
			LeftMargin = new FloatParameter("Отступ слева", 0, 300);
			RightMargin = new FloatParameter("Отступ справа", 0, 300);
			BottomMargin = new FloatParameter("Отступ снизу", 0, 300);
			TopMargin = new FloatParameter("Отступ сверху", 0, 300);
			PageOrientation = new EnumParameter<WdOrientation>("Ориентация страницы");
			_parameters = new List<CheckParameter> {PaperSize, LeftMargin, RightMargin, BottomMargin, TopMargin, PageOrientation};
		}

		[DataMember] public readonly FloatParameter BottomMargin;
		[DataMember] public readonly FloatParameter LeftMargin;
		[DataMember] public readonly EnumParameter<WdOrientation> PageOrientation;
		[DataMember] public readonly EnumParameter<WdPaperSize> PaperSize;
		[DataMember] public readonly FloatParameter RightMargin;
		[DataMember] public readonly FloatParameter TopMargin;

		public override String Name => "Форматирование страницы";

		public override CheckResult Invoke(WordDocument wordDocument, ref BackgroundWorker backgroundWorker)
		{
			var document = wordDocument.GetInteropDocument();
			var pageSetup = document.PageSetup;
			Boolean success = true;
			var occurrences = new List<KeyValuePair<String, List<Paragraph>>>();

			if (PaperSize.Enabled && pageSetup.PaperSize != PaperSize.Value)
			{
				success = false;
				occurrences.Add(new KeyValuePair<String, List<Paragraph>>(Settings.Default.PaperSize, null));
			}

			if (LeftMargin.Enabled && Math.Round(pageSetup.LeftMargin) != Math.Round(Cm2Pts(LeftMargin.Value)))
			{
				success = false;
				occurrences.Add(new KeyValuePair<String, List<Paragraph>>(Settings.Default.LeftMargin, null));
			}
			if (RightMargin.Enabled && Math.Round(pageSetup.RightMargin) != Math.Round(Cm2Pts(RightMargin.Value)))
			{
				success = false;
				occurrences.Add(new KeyValuePair<String, List<Paragraph>>(Settings.Default.RightMargin, null));
			}
			if (BottomMargin.Enabled && Math.Round(pageSetup.BottomMargin) != Math.Round(Cm2Pts(BottomMargin.Value)))
			{
				success = false;
				occurrences.Add(new KeyValuePair<String, List<Paragraph>>(Settings.Default.BottomMargin, null));
			}
			if (TopMargin.Enabled && Math.Round(pageSetup.TopMargin) != Math.Round(Cm2Pts(TopMargin.Value)))
			{
				success = false;
				occurrences.Add(new KeyValuePair<String, List<Paragraph>>(Settings.Default.TopMargin, null));
			}
			if (PageOrientation.Enabled && pageSetup.Orientation != PageOrientation.Value)
			{
				success = false;
				occurrences.Add(new KeyValuePair<String, List<Paragraph>>(Settings.Default.PageOrientation, null));
			}

			return new CheckResult(success, String.Empty, this, occurrences.Count != 0 ? occurrences : null);
		}

		public override IEnumerable<CheckParameter> GetParameters()
		{
			return _parameters;
		}

		public override void CopyValuesFromDeserialized(Check fakeCheck)
		{
			var fakeCheckCasted = (PageFormatCheck) fakeCheck;

			PaperSize.SafeCopyValuesFromDeserialized(fakeCheckCasted.PaperSize);
			LeftMargin.SafeCopyValuesFromDeserialized(fakeCheckCasted.LeftMargin);
			RightMargin.SafeCopyValuesFromDeserialized(fakeCheckCasted.RightMargin);
			BottomMargin.SafeCopyValuesFromDeserialized(fakeCheckCasted.BottomMargin);
			TopMargin.SafeCopyValuesFromDeserialized(fakeCheckCasted.TopMargin);
			PageOrientation.SafeCopyValuesFromDeserialized(fakeCheckCasted.PageOrientation);
		}

		private readonly IEnumerable<CheckParameter> _parameters;
	}
}