﻿using System;
using Microsoft.Office.Interop.Word;
using Novacode;

namespace DocChecker.Core
{
	/// <summary>
	///    Инкапсулирует средства доступа к документам MS Word.
	/// </summary>
	public class WordDocument : IDisposable
	{
		public WordDocument(String path)
		{
			Path = path;
			_interopDocument = new InteropDocument(path);
			_docXDocument = new DocXDocument(path);
		}

		public readonly String Path;

		public void Dispose()
		{
			_interopDocument.Dispose();
			_docXDocument.Dispose();
		}

		public Document GetInteropDocument() => _interopDocument.Document;
		public DocX GetDocXDocument() => _docXDocument.Document;

		public InteropDocument GetInteropDocumentContainer() => _interopDocument;
		public DocXDocument GetDocXDocumentContainer() => _docXDocument;

		private readonly DocXDocument _docXDocument;
		private readonly InteropDocument _interopDocument;
	}

	/// <summary>
	///    Инкапсулирует документ Microsoft.Office.Interop.Word и обеспечивает "ленивый" доступ к нему.
	/// </summary>
	public class InteropDocument : IDisposable
	{
		public InteropDocument(String path)
		{
			_path = path;
		}

		public void Dispose()
		{
			_document?.Close(false);
			_wordApplication?.Quit();
		}

		public Document Document
		{
			get
			{
				if (_document == null)
				{
					_wordApplication = new Application {Visible = false};
					_document = _wordApplication.Documents.Open(_path, ReadOnly: true);
				}

				return _document;
			}
		}

		public StylesCache StylesCache
		{
			get
			{
				if (_document == null)
				{
					_wordApplication = new Application {Visible = false};
					_document = _wordApplication.Documents.Open(_path, ReadOnly: true);
					_stylesCache = new StylesCache(_document);
				}

				return _stylesCache;
			}
		}

		private readonly String _path;
		private Document _document;
		private Application _wordApplication;
		private StylesCache _stylesCache;
	}

	/// <summary>
	///    Инкапсулирует DocX документ и обеспечивает "ленивый" доступ к нему.
	/// </summary>
	public class DocXDocument : IDisposable
	{
		public DocXDocument(String path)
		{
			_path = path;
		}

		public void Dispose()
		{
			_docXDocument?.Dispose();
		}

		public DocX Document => _docXDocument ?? (_docXDocument = DocX.Load(_path));

		private readonly String _path;

		private DocX _docXDocument;
	}
}