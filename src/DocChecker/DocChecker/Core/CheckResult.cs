﻿using System;
using System.Collections.Generic;
using DocChecker.Core.Checks;
using Microsoft.Office.Interop.Word;

namespace DocChecker.Core
{
	/// <summary>
	///    Структура, содержащая результаты проверки.
	/// </summary>
	public struct CheckResult
	{
		public CheckResult(Boolean success, String message, Check check,
			List<KeyValuePair<String, List<Paragraph>>> occurrences = null)
		{
			Check = check;
			Success = success;
			Message = message;
			Occurrences = occurrences;
		}

		public readonly Check Check;
		public readonly String Message;
		public readonly Boolean Success;
		public readonly List<KeyValuePair<String, List<Paragraph>>> Occurrences;
	}
}