﻿using System;

namespace DocChecker.Core
{
	/// <summary>
	///    Управляет открытием и закрытием документов MS Word.
	/// </summary>
	public static class DocumentControl
	{
		public static WordDocument CurrentDocument { get; private set; }
		public static Boolean IsOpened => CurrentDocument != null;

		public static event EventHandler Opened;

		public static WordDocument Open(String path)
		{
			CloseDocument();
			CurrentDocument = new WordDocument(path);
			Opened?.Invoke(null, EventArgs.Empty);

			return CurrentDocument;
		}

		public static void CloseDocument()
		{
			CurrentDocument?.Dispose();
			CurrentDocument = null;
		}
	}
}