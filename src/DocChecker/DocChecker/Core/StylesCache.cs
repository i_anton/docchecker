﻿using System;
using System.Collections.Generic;
using Microsoft.Office.Interop.Word;

namespace DocChecker.Core
{
	/// <summary>
	///    Отвечает за загрузку стилей из документа, фильтрацию и кэширование.
	/// </summary>
	/// <remarks>
	///    Отвечает одновременно и за фильтрацию и за кэширование для простоты.
	/// </remarks>
	public class StylesCache
	{
		public StylesCache(Document document)
		{
			var paragraphs = document.Content.Paragraphs;
			foreach (Paragraph p in paragraphs)
			{
				var pStyle = p.get_Style();
				if (!CacheContains(pStyle.NameLocal))
				{
					var nameLocal = pStyle.NameLocal;
					_namesDictionary.Add(pStyle, nameLocal);
					_stylesDictionary.Add(nameLocal, pStyle);
				}
			}
		}

		public Style this[String nameLocal]
		{
			get
			{
				Style result;
				if (_stylesDictionary.TryGetValue(nameLocal, out result))
					return result;

				throw new StyleNotFoundException();
			}
		}

		public String this[Style cachedStyle]
		{
			get
			{
				String result;
				if (_namesDictionary.TryGetValue(cachedStyle, out result))
					return result;

				throw new StyleNotFoundException();
			}
		}

		public IEnumerable<String> NamesLocal => _namesDictionary.Values;
		public IEnumerable<Style> Styles => _stylesDictionary.Values;

		public Boolean CacheContains(Style style) => _namesDictionary.ContainsKey(style);
		public Boolean CacheContains(String nameLocal) => _stylesDictionary.ContainsKey(nameLocal);

		private readonly Dictionary<Style, String> _namesDictionary = new Dictionary<Style, String>();
		private readonly Dictionary<String, Style> _stylesDictionary = new Dictionary<String, Style>();
	}
}

public class StyleNotFoundException : Exception
{
	public StyleNotFoundException()
	{
	}

	public StyleNotFoundException(String message) : base(message)
	{
	}

	public StyleNotFoundException(String message, Exception innerException) : base(message, innerException)
	{
	}
}