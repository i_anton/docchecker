﻿using System;
using System.Linq;
using DocChecker.Properties;
using Microsoft.Office.Interop.Word;

namespace DocChecker.Core
{
	public static class InteropHelper
	{
		/// <summary>
		///    Получает номер страницы из range
		/// </summary>
		public static Int32 GetPageNumberOfRange(Range range)
		{
			return (Int32) range.Information[WdInformation.wdActiveEndPageNumber];
		}

		/// <summary>
		///    Возвращает первые n символов из абзаца
		/// </summary>
		public static String GetShortedParagraph(Paragraph p)
		{
			String text = p.Range.Text;
			return text.Substring(0,
				text.Length > Settings.Default.ShortParagraphLength ? Settings.Default.ShortParagraphLength : text.Length)
				.Replace("\r", String.Empty).Replace("\n", String.Empty).Replace("\f", " ").Replace("\v", " ").Replace("\t", " ");
		}

		public static Single Cm2Pts(Single x) => (Single) (x * 28.35);
		public static Single Pts2Cm(Single x) => (Single) (x / 28.35);


		/// <summary>
		///    Возвращает стиль с указанным localName или null, если ничего не найдено.
		/// </summary>
		public static Style FindStyleByLocalName(this Document document, String localName)
		{
			return document.Styles.Cast<Style>().FirstOrDefault(style => style.NameLocal == localName);
		}
	}
}