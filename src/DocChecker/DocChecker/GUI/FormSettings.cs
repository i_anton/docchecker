﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace DocChecker.GUI
{
	public partial class FormSettings : Form
	{
		public FormSettings()
		{
			InitializeComponent();
			setGUIValues();
		}

		private void buttonSave_Click(object sender, EventArgs e)
		{
			setSettingsValues();
			Properties.Settings.Default.Save();
			this.Close();
		}

		private void buttonCancel_Click(object sender, EventArgs e)
		{
			this.Close();
		}
		private void setGUIValues()
		{
			checkShowLineNumbers.Checked = Properties.Settings.Default.ShowLineNumbers;
			checkShowSymbols.Checked = Properties.Settings.Default.ShowShortifiedParagraph;
			numericShowNSymbols.Value = Properties.Settings.Default.ShortParagraphLength;
			numericShowNSymbols.Enabled = checkShowSymbols.Checked;
			foreach (TextBox tb in GetAllTextBoxes(flowLayoutPanel4))
			{
				tb.Text = (String)Properties.Settings.Default[tb.Name];
			}
		}
		private IEnumerable<Control> GetAllTextBoxes(Control control)
		{
			var controls = control.Controls.Cast<Control>();

			return controls.SelectMany(ctrl => GetAllTextBoxes(ctrl))
									  .Concat(controls)
									  .Where(c => c.GetType() == typeof(TextBox));
		}
		private void setSettingsValues()
		{
			Properties.Settings.Default.ShowLineNumbers = checkShowLineNumbers.Checked;
			Properties.Settings.Default.ShowShortifiedParagraph = checkShowSymbols.Checked;
			Properties.Settings.Default.ShortParagraphLength = (int)numericShowNSymbols.Value;
			foreach (TextBox tb in GetAllTextBoxes(flowLayoutPanel4))
			{
				Properties.Settings.Default[tb.Name] = tb.Text;
			}
		}

		private void checkShowSymbols_CheckedChanged(object sender, EventArgs e)
		{
			labelNNumbers.Enabled = checkShowSymbols.Checked;
			numericShowNSymbols.Enabled = checkShowSymbols.Checked;
		}

		private void buttonDefaults_Click(object sender, EventArgs e)
		{
			Properties.Settings.Default.Reset();
			setGUIValues();
		}
	}
}
