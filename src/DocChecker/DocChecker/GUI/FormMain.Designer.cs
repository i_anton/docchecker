﻿namespace DocChecker.GUI
{
	partial class FormMain
	{
		/// <summary>
		/// Обязательная переменная конструктора.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Освободить все используемые ресурсы.
		/// </summary>
		/// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Код, автоматически созданный конструктором форм Windows

		/// <summary>
		/// Требуемый метод для поддержки конструктора — не изменяйте 
		/// содержимое этого метода с помощью редактора кода.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
			this.MenuStripMain = new System.Windows.Forms.MenuStrip();
			this.FileMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.openDocumentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.SaveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.LogParametersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.ExitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.checksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.TemplateImportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.TemplateExportStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.addCheckToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.HelpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.AboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.openDocumentDialog = new System.Windows.Forms.OpenFileDialog();
			this.saveLogDialog = new System.Windows.Forms.SaveFileDialog();
			this.fontDialog = new System.Windows.Forms.FontDialog();
			this.openTemplateDialog = new System.Windows.Forms.OpenFileDialog();
			this.saveTemplateDialog = new System.Windows.Forms.SaveFileDialog();
			this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.checksProgressBar = new System.Windows.Forms.ToolStripProgressBar();
			this.StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.CurrentStateLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.checksContainerGroupBox = new System.Windows.Forms.GroupBox();
			this.checksContainer = new System.Windows.Forms.FlowLayoutPanel();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.treeView = new System.Windows.Forms.TreeView();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.LogOutput = new System.Windows.Forms.RichTextBox();
			this.toolStrip1 = new System.Windows.Forms.ToolStrip();
			this.toolStripOpenDoc = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
			this.toolStripCheckDoc = new System.Windows.Forms.ToolStripButton();
			this.toolStripAbort = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
			this.toolStripClearLog = new System.Windows.Forms.ToolStripButton();
			this.MenuStripMain.SuspendLayout();
			this.toolStripContainer1.BottomToolStripPanel.SuspendLayout();
			this.toolStripContainer1.ContentPanel.SuspendLayout();
			this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
			this.toolStripContainer1.SuspendLayout();
			this.statusStrip1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.checksContainerGroupBox.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.tabPage2.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.toolStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// MenuStripMain
			// 
			this.MenuStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.FileMenuItem,
            this.checksToolStripMenuItem,
            this.HelpToolStripMenuItem});
			this.MenuStripMain.Location = new System.Drawing.Point(0, 0);
			this.MenuStripMain.Name = "MenuStripMain";
			this.MenuStripMain.Size = new System.Drawing.Size(990, 24);
			this.MenuStripMain.TabIndex = 0;
			this.MenuStripMain.Text = "menuStrip1";
			// 
			// FileMenuItem
			// 
			this.FileMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openDocumentToolStripMenuItem,
            this.SaveToolStripMenuItem,
            this.LogParametersToolStripMenuItem,
            this.toolStripSeparator1,
            this.ExitToolStripMenuItem});
			this.FileMenuItem.Name = "FileMenuItem";
			this.FileMenuItem.Size = new System.Drawing.Size(48, 20);
			this.FileMenuItem.Text = "Файл";
			// 
			// openDocumentToolStripMenuItem
			// 
			this.openDocumentToolStripMenuItem.Name = "openDocumentToolStripMenuItem";
			this.openDocumentToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
			this.openDocumentToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
			this.openDocumentToolStripMenuItem.Text = "Открыть документ";
			this.openDocumentToolStripMenuItem.Click += new System.EventHandler(this.openDocumentButton_Click);
			// 
			// SaveToolStripMenuItem
			// 
			this.SaveToolStripMenuItem.Name = "SaveToolStripMenuItem";
			this.SaveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
			this.SaveToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
			this.SaveToolStripMenuItem.Text = "Сохранить лог";
			this.SaveToolStripMenuItem.Click += new System.EventHandler(this.SaveToolStripMenuItem_Click);
			// 
			// LogParametersToolStripMenuItem
			// 
			this.LogParametersToolStripMenuItem.Name = "LogParametersToolStripMenuItem";
			this.LogParametersToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
			this.LogParametersToolStripMenuItem.Text = "Параметры лога";
			this.LogParametersToolStripMenuItem.Click += new System.EventHandler(this.LogParametersToolStripMenuItem_Click);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(216, 6);
			// 
			// ExitToolStripMenuItem
			// 
			this.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem";
			this.ExitToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
			this.ExitToolStripMenuItem.Text = "Выход";
			this.ExitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
			// 
			// checksToolStripMenuItem
			// 
			this.checksToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TemplateImportToolStripMenuItem,
            this.TemplateExportStripMenuItem,
            this.toolStripSeparator2,
            this.addCheckToolStripMenuItem});
			this.checksToolStripMenuItem.Name = "checksToolStripMenuItem";
			this.checksToolStripMenuItem.Size = new System.Drawing.Size(74, 20);
			this.checksToolStripMenuItem.Text = "Проверки";
			// 
			// TemplateImportToolStripMenuItem
			// 
			this.TemplateImportToolStripMenuItem.Name = "TemplateImportToolStripMenuItem";
			this.TemplateImportToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
			this.TemplateImportToolStripMenuItem.Text = "Импорт шаблона";
			this.TemplateImportToolStripMenuItem.Click += new System.EventHandler(this.TemplateImportToolStripMenuItem_Click);
			// 
			// TemplateExportStripMenuItem
			// 
			this.TemplateExportStripMenuItem.Name = "TemplateExportStripMenuItem";
			this.TemplateExportStripMenuItem.Size = new System.Drawing.Size(181, 22);
			this.TemplateExportStripMenuItem.Text = "Экспорт шаблона";
			this.TemplateExportStripMenuItem.Click += new System.EventHandler(this.TemplateExportStripMenuItem_Click);
			// 
			// toolStripSeparator2
			// 
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new System.Drawing.Size(178, 6);
			// 
			// addCheckToolStripMenuItem
			// 
			this.addCheckToolStripMenuItem.Name = "addCheckToolStripMenuItem";
			this.addCheckToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
			this.addCheckToolStripMenuItem.Text = "Добавить проверку";
			// 
			// HelpToolStripMenuItem
			// 
			this.HelpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AboutToolStripMenuItem});
			this.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem";
			this.HelpToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
			this.HelpToolStripMenuItem.Text = "Справка";
			// 
			// AboutToolStripMenuItem
			// 
			this.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem";
			this.AboutToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.AboutToolStripMenuItem.Text = "О программе";
			this.AboutToolStripMenuItem.Click += new System.EventHandler(this.AboutToolStripMenuItem_Click);
			// 
			// openDocumentDialog
			// 
			this.openDocumentDialog.FileName = "*.docx";
			this.openDocumentDialog.Filter = "Документы Word|*.docx";
			this.openDocumentDialog.Title = "Открыть документ";
			this.openDocumentDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.openDocumentDialog_FileOk);
			// 
			// saveLogDialog
			// 
			this.saveLogDialog.DefaultExt = "txt";
			this.saveLogDialog.FileName = "*.txt";
			this.saveLogDialog.Filter = "Текстовый документ|*.txt";
			this.saveLogDialog.Title = "Сохранить лог";
			this.saveLogDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.saveLogDialog_FileOk);
			// 
			// fontDialog
			// 
			this.fontDialog.AllowVerticalFonts = false;
			this.fontDialog.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.fontDialog.FontMustExist = true;
			this.fontDialog.ShowEffects = false;
			// 
			// openTemplateDialog
			// 
			this.openTemplateDialog.FileName = "*.json";
			this.openTemplateDialog.Filter = "Файл шаблона|*.json";
			this.openTemplateDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.openTemplateDialog_FileOk);
			// 
			// saveTemplateDialog
			// 
			this.saveTemplateDialog.DefaultExt = "json";
			this.saveTemplateDialog.Filter = "Файл шаблона|*.json";
			this.saveTemplateDialog.Tag = "";
			this.saveTemplateDialog.Title = "Экспорт";
			this.saveTemplateDialog.ValidateNames = false;
			this.saveTemplateDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.saveTemplateDialog_FileOk);
			// 
			// toolStripContainer1
			// 
			// 
			// toolStripContainer1.BottomToolStripPanel
			// 
			this.toolStripContainer1.BottomToolStripPanel.Controls.Add(this.statusStrip1);
			// 
			// toolStripContainer1.ContentPanel
			// 
			this.toolStripContainer1.ContentPanel.Controls.Add(this.splitContainer1);
			this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(990, 365);
			this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.toolStripContainer1.LeftToolStripPanelVisible = false;
			this.toolStripContainer1.Location = new System.Drawing.Point(0, 24);
			this.toolStripContainer1.Name = "toolStripContainer1";
			this.toolStripContainer1.RightToolStripPanelVisible = false;
			this.toolStripContainer1.Size = new System.Drawing.Size(990, 412);
			this.toolStripContainer1.TabIndex = 1;
			this.toolStripContainer1.Text = "toolStripContainer1";
			// 
			// toolStripContainer1.TopToolStripPanel
			// 
			this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip1);
			// 
			// statusStrip1
			// 
			this.statusStrip1.Dock = System.Windows.Forms.DockStyle.None;
			this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.checksProgressBar,
            this.StatusLabel,
            this.CurrentStateLabel});
			this.statusStrip1.Location = new System.Drawing.Point(0, 0);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Size = new System.Drawing.Size(990, 22);
			this.statusStrip1.TabIndex = 1;
			// 
			// checksProgressBar
			// 
			this.checksProgressBar.Name = "checksProgressBar";
			this.checksProgressBar.Size = new System.Drawing.Size(100, 16);
			// 
			// StatusLabel
			// 
			this.StatusLabel.Name = "StatusLabel";
			this.StatusLabel.Size = new System.Drawing.Size(69, 17);
			this.StatusLabel.Text = "Состояние:";
			// 
			// CurrentStateLabel
			// 
			this.CurrentStateLabel.Name = "CurrentStateLabel";
			this.CurrentStateLabel.Size = new System.Drawing.Size(64, 17);
			this.CurrentStateLabel.Text = "Ожидание";
			// 
			// splitContainer1
			// 
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.Location = new System.Drawing.Point(0, 0);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.checksContainerGroupBox);
			this.splitContainer1.Panel1MinSize = 441;
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.tabControl1);
			this.splitContainer1.Size = new System.Drawing.Size(990, 365);
			this.splitContainer1.SplitterDistance = 517;
			this.splitContainer1.TabIndex = 2;
			// 
			// checksContainerGroupBox
			// 
			this.checksContainerGroupBox.Controls.Add(this.checksContainer);
			this.checksContainerGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.checksContainerGroupBox.Location = new System.Drawing.Point(0, 0);
			this.checksContainerGroupBox.Name = "checksContainerGroupBox";
			this.checksContainerGroupBox.Size = new System.Drawing.Size(517, 365);
			this.checksContainerGroupBox.TabIndex = 2;
			this.checksContainerGroupBox.TabStop = false;
			this.checksContainerGroupBox.Text = "Проверки";
			// 
			// checksContainer
			// 
			this.checksContainer.AutoScroll = true;
			this.checksContainer.Dock = System.Windows.Forms.DockStyle.Fill;
			this.checksContainer.Location = new System.Drawing.Point(3, 16);
			this.checksContainer.Name = "checksContainer";
			this.checksContainer.Size = new System.Drawing.Size(511, 346);
			this.checksContainer.TabIndex = 0;
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl1.Location = new System.Drawing.Point(0, 0);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(469, 365);
			this.tabControl1.TabIndex = 0;
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.treeView);
			this.tabPage2.Location = new System.Drawing.Point(4, 22);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage2.Size = new System.Drawing.Size(461, 339);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "Дерево";
			this.tabPage2.UseVisualStyleBackColor = true;
			// 
			// treeView
			// 
			this.treeView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.treeView.Location = new System.Drawing.Point(3, 3);
			this.treeView.Name = "treeView";
			this.treeView.Size = new System.Drawing.Size(455, 333);
			this.treeView.TabIndex = 0;
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.LogOutput);
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage1.Size = new System.Drawing.Size(461, 339);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "Лог";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// LogOutput
			// 
			this.LogOutput.BackColor = System.Drawing.SystemColors.Window;
			this.LogOutput.Dock = System.Windows.Forms.DockStyle.Fill;
			this.LogOutput.Location = new System.Drawing.Point(3, 3);
			this.LogOutput.Name = "LogOutput";
			this.LogOutput.Size = new System.Drawing.Size(455, 333);
			this.LogOutput.TabIndex = 1;
			this.LogOutput.Text = "";
			// 
			// toolStrip1
			// 
			this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
			this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
			this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripOpenDoc,
            this.toolStripSeparator3,
            this.toolStripCheckDoc,
            this.toolStripAbort,
            this.toolStripSeparator4,
            this.toolStripClearLog});
			this.toolStrip1.Location = new System.Drawing.Point(3, 0);
			this.toolStrip1.Name = "toolStrip1";
			this.toolStrip1.Size = new System.Drawing.Size(107, 25);
			this.toolStrip1.TabIndex = 0;
			// 
			// toolStripOpenDoc
			// 
			this.toolStripOpenDoc.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolStripOpenDoc.Image = global::DocChecker.Properties.Resources.open;
			this.toolStripOpenDoc.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripOpenDoc.Name = "toolStripOpenDoc";
			this.toolStripOpenDoc.Size = new System.Drawing.Size(23, 22);
			this.toolStripOpenDoc.Text = "Открыть документ";
			this.toolStripOpenDoc.Click += new System.EventHandler(this.openDocumentButton_Click);
			// 
			// toolStripSeparator3
			// 
			this.toolStripSeparator3.Name = "toolStripSeparator3";
			this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
			// 
			// toolStripCheckDoc
			// 
			this.toolStripCheckDoc.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolStripCheckDoc.Enabled = false;
			this.toolStripCheckDoc.Image = global::DocChecker.Properties.Resources.start;
			this.toolStripCheckDoc.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripCheckDoc.Name = "toolStripCheckDoc";
			this.toolStripCheckDoc.Size = new System.Drawing.Size(23, 22);
			this.toolStripCheckDoc.Text = "Проверка";
			this.toolStripCheckDoc.ToolTipText = "Начать проверку документа";
			this.toolStripCheckDoc.Click += new System.EventHandler(this.CheckButton_Click);
			// 
			// toolStripAbort
			// 
			this.toolStripAbort.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolStripAbort.Enabled = false;
			this.toolStripAbort.Image = global::DocChecker.Properties.Resources.stop;
			this.toolStripAbort.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripAbort.Name = "toolStripAbort";
			this.toolStripAbort.Size = new System.Drawing.Size(23, 22);
			this.toolStripAbort.Text = "Прервать";
			this.toolStripAbort.ToolTipText = "Прервать проверку";
			this.toolStripAbort.Click += new System.EventHandler(this.AbortCheck_Click);
			// 
			// toolStripSeparator4
			// 
			this.toolStripSeparator4.Name = "toolStripSeparator4";
			this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
			// 
			// toolStripClearLog
			// 
			this.toolStripClearLog.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.toolStripClearLog.Image = global::DocChecker.Properties.Resources.clear;
			this.toolStripClearLog.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.toolStripClearLog.Name = "toolStripClearLog";
			this.toolStripClearLog.Size = new System.Drawing.Size(23, 22);
			this.toolStripClearLog.Text = "Очистить";
			this.toolStripClearLog.Click += new System.EventHandler(this.ClearLogButton_Click);
			// 
			// FormMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(990, 436);
			this.Controls.Add(this.toolStripContainer1);
			this.Controls.Add(this.MenuStripMain);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MainMenuStrip = this.MenuStripMain;
			this.MinimumSize = new System.Drawing.Size(580, 370);
			this.Name = "FormMain";
			this.Text = "Проверка форматирования документов";
			this.MenuStripMain.ResumeLayout(false);
			this.MenuStripMain.PerformLayout();
			this.toolStripContainer1.BottomToolStripPanel.ResumeLayout(false);
			this.toolStripContainer1.BottomToolStripPanel.PerformLayout();
			this.toolStripContainer1.ContentPanel.ResumeLayout(false);
			this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
			this.toolStripContainer1.TopToolStripPanel.PerformLayout();
			this.toolStripContainer1.ResumeLayout(false);
			this.toolStripContainer1.PerformLayout();
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.checksContainerGroupBox.ResumeLayout(false);
			this.tabControl1.ResumeLayout(false);
			this.tabPage2.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.toolStrip1.ResumeLayout(false);
			this.toolStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.MenuStrip MenuStripMain;
		private System.Windows.Forms.ToolStripMenuItem FileMenuItem;
		private System.Windows.Forms.ToolStripMenuItem openDocumentToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem SaveToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem ExitToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem checksToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem TemplateImportToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem TemplateExportStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem HelpToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem AboutToolStripMenuItem;
		private System.Windows.Forms.OpenFileDialog openDocumentDialog;
		private System.Windows.Forms.SaveFileDialog saveLogDialog;
		private System.Windows.Forms.FontDialog fontDialog;
		private System.Windows.Forms.OpenFileDialog openTemplateDialog;
		private System.Windows.Forms.SaveFileDialog saveTemplateDialog;
		private System.Windows.Forms.ToolStripMenuItem addCheckToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem LogParametersToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripContainer toolStripContainer1;
		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.TreeView treeView;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.RichTextBox LogOutput;
		private System.Windows.Forms.StatusStrip statusStrip1;
		private System.Windows.Forms.ToolStripStatusLabel StatusLabel;
		private System.Windows.Forms.ToolStripStatusLabel CurrentStateLabel;
		private System.Windows.Forms.ToolStripProgressBar checksProgressBar;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
		private System.Windows.Forms.ToolStrip toolStrip1;
		private System.Windows.Forms.ToolStripButton toolStripOpenDoc;
		private System.Windows.Forms.ToolStripButton toolStripCheckDoc;
		private System.Windows.Forms.ToolStripButton toolStripAbort;
		private System.Windows.Forms.ToolStripButton toolStripClearLog;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
		private System.Windows.Forms.GroupBox checksContainerGroupBox;
		private System.Windows.Forms.FlowLayoutPanel checksContainer;
	}
}

