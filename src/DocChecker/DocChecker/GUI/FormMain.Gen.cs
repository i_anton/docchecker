﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DocChecker.Core;
using DocChecker.Core.Checks;
using DocChecker.Core.Checks.Parameters;
using DocChecker.Properties;

namespace DocChecker.GUI
{
	public partial class FormMain
	{
		private const Int32 ParameterBoxLength = 500;

		private void OnCheckListClear(Object sender, EventArgs e)
		{
			foreach (Control control in checksContainer.Controls)
				control.Dispose();

			checksContainer.Controls.Clear();
			_controlsDictionary.Clear();
		}

		private void OnCheckListCheckAdded(CheckListEventArgs e)
		{
			var check = e.Check;

			var groupBox = new GroupBox
			{
				Dock = DockStyle.Right,
				AutoSize = true,
				AutoSizeMode = AutoSizeMode.GrowOnly,
				Text = check.Name,
				Padding = new Padding(4, 10, 4, 4)
			};

			var flowLayoutPanel = new FlowLayoutPanel
			{
				Dock = DockStyle.Fill,
				AutoSize = true,
				FlowDirection = FlowDirection.TopDown
			};

			var deleteButton = new Button
			{
				Size = new Size(24, 23),
				Text = @"x",
				Location = new Point(ParameterBoxLength - 50, 1)
			};
			deleteButton.Click += (bSender, bE) => CheckList.Remove(check);

			var minimizeButton = new Button
			{
				Size = new Size(24, 23),
				Text = @"-",
				Location = new Point(ParameterBoxLength - 75, 1)
			};
			minimizeButton.Click += (bSender, bE) =>
			{
				flowLayoutPanel.Visible = !flowLayoutPanel.Visible;
				minimizeButton.Text = flowLayoutPanel.Visible ? @"-" : "\u25FB";
			};

			groupBox.Controls.Add(flowLayoutPanel);
			groupBox.Controls.Add(deleteButton);
			groupBox.Controls.Add(minimizeButton);
			checksContainer.Controls.Add(groupBox);

			check.NameChanged += (bSender, bE) => groupBox.Text = check.Name;

			foreach (var checkParameter in check.GetParameters())
			{
				flowLayoutPanel.Controls.Add(GetParameterContainer(checkParameter));
			}

			_controlsDictionary.Add(check, groupBox);
		}

		private void OnCheckListCheckRemoved(CheckListEventArgs e)
		{
			var checkContainer = _controlsDictionary[e.Check];
			checkContainer.Dispose();

			checksContainer.Controls.Remove(checkContainer);
			_controlsDictionary.Remove(e.Check);
		}

		private Control GetParameterContainer(CheckParameter parameter)
		{
			if (parameter is FloatParameter)
				return GetFloatParameterContainer((FloatParameter) parameter);
			if (parameter is IntegerParameter)
				return GetIntegerParameterContainer((IntegerParameter) parameter);
			if (parameter.GetType().IsGenericType && parameter.GetType().GetGenericTypeDefinition() == typeof(EnumParameter<>))
				return GetEnumParameterContainer(parameter);
			if (parameter is FontParameter)
				return GetFontParameterContainer((FontParameter) parameter);
			if (parameter is BooleanParameter)
				return GetBooleanParameterContainer((BooleanParameter) parameter);
			if (parameter is StyleParameter)
				return GetStyleParameterContainer((StyleParameter) parameter);

			ApplicationLog.LogError(
				$"Не удалось отобразить параметр \"{parameter.Name}\", значение недоступно для изменения из интерфейса.");
			return new GroupBox {Size = new Size(50, 10)};
		}

		private Control GetFloatParameterContainer(FloatParameter parameter)
		{
			var flowLayoutPanel = new FlowLayoutPanel
			{
				AutoSize = true
			};
			var checkBox = new CheckBox
			{
				AutoSize = false,
				Size = new Size(ParameterBoxLength - 140, 17),
				Text = parameter.Name,
				UseVisualStyleBackColor = true
			};
			var numericUpDown = new NumericUpDown
			{
				Size = new Size(60, 20),
				Minimum = (Decimal) parameter.MinValue,
				Maximum = (Decimal) parameter.MaxValue,
				Increment = (Decimal) 0.1, //Как в ворде
				DecimalPlaces = 1
			};

			EventHandler onCheckBoxCheckedChanged = (sender, e) =>
			{
				numericUpDown.Enabled = checkBox.Checked;
				parameter.Enabled = checkBox.Checked;
			};
			EventHandler onParameterEnabledChanged = (sender, e) => checkBox.Checked = parameter.Enabled;
			EventHandler onNumericUpDownValueChanged = (sender, e) => parameter.Value = (Single) numericUpDown.Value;
			EventHandler onParameterValueChanged = (sender, e) => numericUpDown.Value = (Decimal) parameter.Value;
			EventHandler onEnabledDepenceParameterEnabledChanged =
				(sender, e) => checkBox.Enabled = parameter.EnabledDependenceParameter?.Enabled ?? true;

			checkBox.CheckedChanged += onCheckBoxCheckedChanged;
			parameter.EnabledChanged += onParameterEnabledChanged;
			if (parameter.EnabledDependenceParameter != null)
				parameter.EnabledDependenceParameter.EnabledChanged += onEnabledDepenceParameterEnabledChanged;

			numericUpDown.ValueChanged += onNumericUpDownValueChanged;
			parameter.ValueChanged += onParameterValueChanged;

			flowLayoutPanel.Controls.Add(checkBox);
			flowLayoutPanel.Controls.Add(numericUpDown);

			onParameterEnabledChanged(this, EventArgs.Empty);
			onCheckBoxCheckedChanged(this, EventArgs.Empty); //Выставление контролам правильного состояния
			onEnabledDepenceParameterEnabledChanged(this, EventArgs.Empty);
			onParameterValueChanged(this, EventArgs.Empty);

			return flowLayoutPanel;
		}

		private Control GetIntegerParameterContainer(IntegerParameter parameter)
		{
			var flowLayoutPanel = new FlowLayoutPanel
			{
				AutoSize = true
			};
			var checkBox = new CheckBox
			{
				AutoSize = false,
				Size = new Size(ParameterBoxLength - 140, 17),
				Text = parameter.Name,
				UseVisualStyleBackColor = true
			};
			var numericUpDown = new NumericUpDown
			{
				Size = new Size(60, 20),
				Minimum = parameter.MinValue,
				Maximum = parameter.MaxValue
			};

			EventHandler onCheckBoxCheckedChanged = (sender, e) =>
			{
				numericUpDown.Enabled = checkBox.Checked;
				parameter.Enabled = checkBox.Checked;
			};
			EventHandler onParameterEnabledChanged = (sender, e) => checkBox.Checked = parameter.Enabled;
			EventHandler onNumericUpDownValueChanged = (sender, e) => parameter.Value = (Int32) numericUpDown.Value;
			EventHandler onParameterValueChanged = (sender, e) => numericUpDown.Value = parameter.Value;
			EventHandler onEnabledDepenceParameterEnabledChanged =
				(sender, e) => checkBox.Enabled = parameter.EnabledDependenceParameter?.Enabled ?? true;

			checkBox.CheckedChanged += onCheckBoxCheckedChanged;
			parameter.EnabledChanged += onParameterEnabledChanged;
			if (parameter.EnabledDependenceParameter != null)
				parameter.EnabledDependenceParameter.EnabledChanged += onEnabledDepenceParameterEnabledChanged;

			numericUpDown.ValueChanged += onNumericUpDownValueChanged;
			parameter.ValueChanged += onParameterValueChanged;

			flowLayoutPanel.Controls.Add(checkBox);
			flowLayoutPanel.Controls.Add(numericUpDown);

			onParameterEnabledChanged(this, EventArgs.Empty);
			onCheckBoxCheckedChanged(this, EventArgs.Empty); //Выставление контролам правильного состояния
			onEnabledDepenceParameterEnabledChanged(this, EventArgs.Empty);
			onParameterValueChanged(this, EventArgs.Empty);

			return flowLayoutPanel;
		}

		private Control GetEnumParameterContainer(CheckParameter parameter)
		{
			var flowLayoutPanel = new FlowLayoutPanel
			{
				AutoSize = true
			};
			var checkBox = new CheckBox
			{
				AutoSize = false,
				Text = parameter.Name,
				UseVisualStyleBackColor = true
			};
			var comboBox = new ComboBox {DropDownStyle = ComboBoxStyle.DropDownList};
			var valueProperty = parameter.GetType().GetProperty("Value");
			foreach (String enumName in valueProperty.GetMethod.ReturnType.GetEnumNames())
				try
				{
					comboBox.Items.Add(EnumParameter<Enum>.SimplifyEnumName(enumName));
				}
				catch (Exception)
				{
				}
			//generic параметр ни на что не влияет, но нужно указать

			Int32 maxLength = (from String label in comboBox.Items select label.Length).Max();
			comboBox.Size = new Size(maxLength * 5 + 40, 21);
			checkBox.Size = new Size(ParameterBoxLength - comboBox.Size.Width - 80, 17);
			//А встроенный автосайз мог бы и работать, между прочим

			EventHandler onCheckBoxCheckedChanged = (sender, e) =>
			{
				parameter.Enabled = checkBox.Checked;
				comboBox.Enabled = checkBox.Checked;
			};
			EventHandler onParameterEnabledChanged = (sender, e) =>
				checkBox.Checked = parameter.Enabled;
			EventHandler onComboBoxValueChanged = (sender, e) => valueProperty.SetValue(
				parameter,
				Enum.Parse(valueProperty.PropertyType,
					EnumParameter<Enum>.GetEnumNameFromSimplifiedName(comboBox.Text)));
			EventHandler onParameterValueChanged =
				(sender, e) =>
					comboBox.Text = EnumParameter<Enum>.SimplifyEnumName(valueProperty.GetValue(parameter).ToString());
			EventHandler onEnabledDepenceParameterEnabledChanged =
				(sender, e) => checkBox.Enabled = parameter.EnabledDependenceParameter?.Enabled ?? true;


			checkBox.CheckedChanged += onCheckBoxCheckedChanged;
			parameter.EnabledChanged += onParameterEnabledChanged;
			if (parameter.EnabledDependenceParameter != null)
				parameter.EnabledDependenceParameter.EnabledChanged += onEnabledDepenceParameterEnabledChanged;

			comboBox.SelectedIndexChanged += onComboBoxValueChanged;
			parameter.ValueChanged += onParameterValueChanged;

			flowLayoutPanel.Controls.Add(checkBox);
			flowLayoutPanel.Controls.Add(comboBox);

			onParameterEnabledChanged(this, EventArgs.Empty);
			onCheckBoxCheckedChanged(this, EventArgs.Empty); //Выставление контролам правильного состояния
			onEnabledDepenceParameterEnabledChanged(this, EventArgs.Empty);
			onParameterValueChanged(this, EventArgs.Empty);

			return flowLayoutPanel;
		}

		private Control GetFontParameterContainer(FontParameter parameter)
		{
			var flowLayoutPanel = new FlowLayoutPanel
			{
				AutoSize = true
			};
			var checkBox = new CheckBox
			{
				AutoSize = false,
				Size = new Size(ParameterBoxLength - 236, 17),
				Text = parameter.Name,
				UseVisualStyleBackColor = true
			};
			var textBoxName = new TextBox {ReadOnly = true};
			var textBoxSize = new TextBox {ReadOnly = true, Size = new Size(20, 20)};
			var button = new Button
			{
				Size = new Size(24, 23),
				Text = @"..."
			};


			EventHandler onCheckBoxCheckedChanged = (sender, e) =>
			{
				parameter.Enabled = checkBox.Checked;
				textBoxName.Enabled = checkBox.Enabled;
				textBoxSize.Enabled = checkBox.Enabled;
				button.Enabled = checkBox.Checked;
			};
			EventHandler onParameterEnabledChanged = (sender, e) => checkBox.Checked = parameter.Enabled;
			EventHandler onButtonClicked = (sender, e) =>
			{
				if (fontDialog.ShowDialog() == DialogResult.OK)
					parameter.Value = fontDialog.Font;
			};
			EventHandler onParameterValueChanged = (sender, e) =>
			{
				textBoxName.Text = parameter.Value?.Name;
				textBoxSize.Text = Convert.ToInt32(parameter.Value?.SizeInPoints).ToString();
			};
			EventHandler onEnabledDepenceParameterEnabledChanged =
				(sender, e) => checkBox.Enabled = parameter.EnabledDependenceParameter?.Enabled ?? true;

			checkBox.CheckedChanged += onCheckBoxCheckedChanged;
			parameter.EnabledChanged += onParameterEnabledChanged;
			if (parameter.EnabledDependenceParameter != null)
				parameter.EnabledDependenceParameter.EnabledChanged += onEnabledDepenceParameterEnabledChanged;

			button.Click += onButtonClicked;
			parameter.ValueChanged += onParameterValueChanged;

			flowLayoutPanel.Controls.Add(checkBox);
			flowLayoutPanel.Controls.Add(textBoxName);
			flowLayoutPanel.Controls.Add(textBoxSize);
			flowLayoutPanel.Controls.Add(button);

			onParameterEnabledChanged(this, EventArgs.Empty);
			onCheckBoxCheckedChanged(this, EventArgs.Empty); //Выставление контролам правильного состояния
			onEnabledDepenceParameterEnabledChanged(this, EventArgs.Empty);
			onParameterValueChanged(this, EventArgs.Empty);

			return flowLayoutPanel;
		}

		private Control GetStyleParameterContainer(StyleParameter parameter)
		{
			var flowLayoutPanel = new FlowLayoutPanel
			{
				AutoSize = true
			};
			var checkBox = new CheckBox
			{
				AutoSize = false,
				Text = parameter.Name,
				UseVisualStyleBackColor = true
			};
			var comboBox = new ComboBox
			{
				DropDownStyle = ComboBoxStyle.DropDown
			};

			var warningLabel = new Label
			{
				ForeColor = Color.DarkRed,
				TextAlign = ContentAlignment.MiddleLeft,
				AutoSize = false,
				Size = new Size(95, 26)
			};

			Action updateSizes = () =>
			{
				Int32 maxLength = comboBox.Items.Count > 0
					? (from String label in comboBox.Items select label.Length).Max()
					: 0;
				maxLength = Math.Max(maxLength, comboBox.Text.Length);
				Int32 pointsLength = Math.Max(maxLength * 5 + 40, 200);

				comboBox.Size = new Size(pointsLength, 21);
				checkBox.Size = new Size(ParameterBoxLength - comboBox.Size.Width - warningLabel.Width - 80, 17);
			};

			EventHandler onCheckBoxCheckedChanged = (sender, e) =>
			{
				parameter.Enabled = checkBox.Checked;
				comboBox.Enabled = checkBox.Checked;
			};
			EventHandler onParameterEnabledChanged = (sender, e) =>
				checkBox.Checked = parameter.Enabled;
			EventHandler onComboBoxValueChanged = (sender, e) =>
			{
				try
				{
					parameter.StringValue = comboBox.Text;
					warningLabel.Text = String.Empty; //Пришлось использовать вместо Visible из-за особенностей работы flowLayout
				}
				catch (StyleNotFoundException)
				{
					if (DocumentControl.IsOpened)
						warningLabel.Text = Resources.FormMain_GetStyleParameterContainer_StyleNotFound;
				}
			};
			EventHandler onParameterValueChanged =
				(sender, e) =>
				{
					comboBox.Text = parameter.StringValue;
					updateSizes.Invoke();
				};
			EventHandler onEnabledDepenceParameterEnabledChanged =
				(sender, e) => checkBox.Enabled = parameter.EnabledDependenceParameter?.Enabled ?? true;

			EventHandler updateCombobox = (sender, e) =>
			{
				if (!DocumentControl.IsOpened)
				{
					comboBox.Items.Clear();
					return;
				}

				var filteredStylesNames = DocumentControl.CurrentDocument.GetInteropDocumentContainer().StylesCache.NamesLocal;
				comboBox.Items.Clear();

				foreach (String styleNameLocal in filteredStylesNames)
					comboBox.Items.Add(styleNameLocal);

				updateSizes.Invoke();
				onComboBoxValueChanged.Invoke(comboBox, EventArgs.Empty);
			};

			checkBox.CheckedChanged += onCheckBoxCheckedChanged;
			parameter.EnabledChanged += onParameterEnabledChanged;
			if (parameter.EnabledDependenceParameter != null)
				parameter.EnabledDependenceParameter.EnabledChanged += onEnabledDepenceParameterEnabledChanged;

			comboBox.LostFocus += onComboBoxValueChanged;
			comboBox.SelectedIndexChanged += onComboBoxValueChanged;

			parameter.ValueChanged += onParameterValueChanged;

			DocumentControl.Opened += updateCombobox;
			comboBox.Disposed += (sender, e) => DocumentControl.Opened -= updateCombobox;

			flowLayoutPanel.Controls.Add(checkBox);
			flowLayoutPanel.Controls.Add(warningLabel);
			flowLayoutPanel.Controls.Add(comboBox);

			updateCombobox.Invoke(this, EventArgs.Empty);
			onParameterEnabledChanged.Invoke(this, EventArgs.Empty);
			onEnabledDepenceParameterEnabledChanged.Invoke(this, EventArgs.Empty);
			onParameterValueChanged.Invoke(this, EventArgs.Empty);

			return flowLayoutPanel;
		}

		private Control GetBooleanParameterContainer(BooleanParameter parameter)
		{
			var flowLayoutPanel = new FlowLayoutPanel
			{
				AutoSize = true
			};
			var checkBox = new CheckBox
			{
				AutoSize = false,
				Size = new Size(ParameterBoxLength - 80, 17),
				Text = parameter.Name,
				UseVisualStyleBackColor = true
			};

			EventHandler onCheckBoxCheckedChanged = (sender, e) => parameter.Enabled = checkBox.Checked;
			EventHandler onParameterEnabledChanged = (sender, e) => checkBox.Checked = parameter.Enabled;
			EventHandler onEnabledDepenceParameterEnabledChanged =
				(sender, e) => checkBox.Enabled = parameter.EnabledDependenceParameter?.Enabled ?? true;

			checkBox.CheckedChanged += onCheckBoxCheckedChanged;
			parameter.EnabledChanged += onParameterEnabledChanged;
			if (parameter.EnabledDependenceParameter != null)
				parameter.EnabledDependenceParameter.EnabledChanged += onEnabledDepenceParameterEnabledChanged;

			Disposed += (sender, e) => //WARNING: MEMORY LEAKS DEFENCE
			{
				checkBox.CheckedChanged -= onCheckBoxCheckedChanged;
				parameter.EnabledChanged -= onParameterEnabledChanged;
				if (parameter.EnabledDependenceParameter != null)
					parameter.EnabledDependenceParameter.EnabledChanged -= onEnabledDepenceParameterEnabledChanged;
			};

			flowLayoutPanel.Controls.Add(checkBox);

			onParameterEnabledChanged(this, EventArgs.Empty);
			onCheckBoxCheckedChanged(this, EventArgs.Empty); //Выставление контролам правильного состояния
			onEnabledDepenceParameterEnabledChanged(this, EventArgs.Empty);

			return flowLayoutPanel;
		}

		private readonly Dictionary<Check, Control> _controlsDictionary = new Dictionary<Check, Control>();
	}
}