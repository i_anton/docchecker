﻿namespace DocChecker.GUI
{
	partial class FormSettings
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSettings));
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
			this.buttonDefaults = new System.Windows.Forms.Button();
			this.buttonCancel = new System.Windows.Forms.Button();
			this.buttonSave = new System.Windows.Forms.Button();
			this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
			this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
			this.checkShowLineNumbers = new System.Windows.Forms.CheckBox();
			this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
			this.checkShowSymbols = new System.Windows.Forms.CheckBox();
			this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
			this.labelNNumbers = new System.Windows.Forms.Label();
			this.numericShowNSymbols = new System.Windows.Forms.NumericUpDown();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
			this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
			this.label1 = new System.Windows.Forms.Label();
			this.LeftMargin = new System.Windows.Forms.TextBox();
			this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
			this.label2 = new System.Windows.Forms.Label();
			this.RightMargin = new System.Windows.Forms.TextBox();
			this.flowLayoutPanel10 = new System.Windows.Forms.FlowLayoutPanel();
			this.label3 = new System.Windows.Forms.Label();
			this.BottomMargin = new System.Windows.Forms.TextBox();
			this.flowLayoutPanel11 = new System.Windows.Forms.FlowLayoutPanel();
			this.label4 = new System.Windows.Forms.Label();
			this.TopMargin = new System.Windows.Forms.TextBox();
			this.flowLayoutPanel12 = new System.Windows.Forms.FlowLayoutPanel();
			this.label5 = new System.Windows.Forms.Label();
			this.PageOrientation = new System.Windows.Forms.TextBox();
			this.flowLayoutPanel13 = new System.Windows.Forms.FlowLayoutPanel();
			this.label6 = new System.Windows.Forms.Label();
			this.PaperSize = new System.Windows.Forms.TextBox();
			this.flowLayoutPanel14 = new System.Windows.Forms.FlowLayoutPanel();
			this.label7 = new System.Windows.Forms.Label();
			this.ParagraphAlign = new System.Windows.Forms.TextBox();
			this.flowLayoutPanel15 = new System.Windows.Forms.FlowLayoutPanel();
			this.label8 = new System.Windows.Forms.Label();
			this.ParagraphSpacing = new System.Windows.Forms.TextBox();
			this.flowLayoutPanel16 = new System.Windows.Forms.FlowLayoutPanel();
			this.label9 = new System.Windows.Forms.Label();
			this.ParagraphIndent = new System.Windows.Forms.TextBox();
			this.flowLayoutPanel17 = new System.Windows.Forms.FlowLayoutPanel();
			this.label10 = new System.Windows.Forms.Label();
			this.ParagraphHangingLines = new System.Windows.Forms.TextBox();
			this.flowLayoutPanel18 = new System.Windows.Forms.FlowLayoutPanel();
			this.label11 = new System.Windows.Forms.Label();
			this.NumberedListLevel1 = new System.Windows.Forms.TextBox();
			this.flowLayoutPanel19 = new System.Windows.Forms.FlowLayoutPanel();
			this.label12 = new System.Windows.Forms.Label();
			this.NumberedListLevel2 = new System.Windows.Forms.TextBox();
			this.flowLayoutPanel20 = new System.Windows.Forms.FlowLayoutPanel();
			this.label13 = new System.Windows.Forms.Label();
			this.BulletedList = new System.Windows.Forms.TextBox();
			this.flowLayoutPanel21 = new System.Windows.Forms.FlowLayoutPanel();
			this.label14 = new System.Windows.Forms.Label();
			this.FontFont = new System.Windows.Forms.TextBox();
			this.flowLayoutPanel22 = new System.Windows.Forms.FlowLayoutPanel();
			this.label15 = new System.Windows.Forms.Label();
			this.FontSize = new System.Windows.Forms.TextBox();
			this.flowLayoutPanel23 = new System.Windows.Forms.FlowLayoutPanel();
			this.label16 = new System.Windows.Forms.Label();
			this.FontBlack = new System.Windows.Forms.TextBox();
			this.flowLayoutPanel24 = new System.Windows.Forms.FlowLayoutPanel();
			this.label17 = new System.Windows.Forms.Label();
			this.FontNotStylized = new System.Windows.Forms.TextBox();
			this.flowLayoutPanel25 = new System.Windows.Forms.FlowLayoutPanel();
			this.label18 = new System.Windows.Forms.Label();
			this.IsNumerated = new System.Windows.Forms.TextBox();
			this.flowLayoutPanel26 = new System.Windows.Forms.FlowLayoutPanel();
			this.label19 = new System.Windows.Forms.Label();
			this.IsNotRestarted = new System.Windows.Forms.TextBox();
			this.flowLayoutPanel27 = new System.Windows.Forms.FlowLayoutPanel();
			this.label20 = new System.Windows.Forms.Label();
			this.NoNumerationTop = new System.Windows.Forms.TextBox();
			this.flowLayoutPanel28 = new System.Windows.Forms.FlowLayoutPanel();
			this.label21 = new System.Windows.Forms.Label();
			this.NumerationStyle = new System.Windows.Forms.TextBox();
			this.flowLayoutPanel29 = new System.Windows.Forms.FlowLayoutPanel();
			this.label22 = new System.Windows.Forms.Label();
			this.NumerationAlign = new System.Windows.Forms.TextBox();
			this.flowLayoutPanel30 = new System.Windows.Forms.FlowLayoutPanel();
			this.label23 = new System.Windows.Forms.Label();
			this.DifferentFirstPage = new System.Windows.Forms.TextBox();
			this.flowLayoutPanel31 = new System.Windows.Forms.FlowLayoutPanel();
			this.label24 = new System.Windows.Forms.Label();
			this.DifferentLastPage = new System.Windows.Forms.TextBox();
			this.tableLayoutPanel1.SuspendLayout();
			this.flowLayoutPanel1.SuspendLayout();
			this.flowLayoutPanel2.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.flowLayoutPanel3.SuspendLayout();
			this.flowLayoutPanel5.SuspendLayout();
			this.flowLayoutPanel6.SuspendLayout();
			this.flowLayoutPanel7.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericShowNSymbols)).BeginInit();
			this.groupBox2.SuspendLayout();
			this.flowLayoutPanel4.SuspendLayout();
			this.flowLayoutPanel8.SuspendLayout();
			this.flowLayoutPanel9.SuspendLayout();
			this.flowLayoutPanel10.SuspendLayout();
			this.flowLayoutPanel11.SuspendLayout();
			this.flowLayoutPanel12.SuspendLayout();
			this.flowLayoutPanel13.SuspendLayout();
			this.flowLayoutPanel14.SuspendLayout();
			this.flowLayoutPanel15.SuspendLayout();
			this.flowLayoutPanel16.SuspendLayout();
			this.flowLayoutPanel17.SuspendLayout();
			this.flowLayoutPanel18.SuspendLayout();
			this.flowLayoutPanel19.SuspendLayout();
			this.flowLayoutPanel20.SuspendLayout();
			this.flowLayoutPanel21.SuspendLayout();
			this.flowLayoutPanel22.SuspendLayout();
			this.flowLayoutPanel23.SuspendLayout();
			this.flowLayoutPanel24.SuspendLayout();
			this.flowLayoutPanel25.SuspendLayout();
			this.flowLayoutPanel26.SuspendLayout();
			this.flowLayoutPanel27.SuspendLayout();
			this.flowLayoutPanel28.SuspendLayout();
			this.flowLayoutPanel29.SuspendLayout();
			this.flowLayoutPanel30.SuspendLayout();
			this.flowLayoutPanel31.SuspendLayout();
			this.SuspendLayout();
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 1;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel2, 0, 0);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 2;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel1.Size = new System.Drawing.Size(384, 340);
			this.tableLayoutPanel1.TabIndex = 0;
			// 
			// flowLayoutPanel1
			// 
			this.flowLayoutPanel1.AutoSize = true;
			this.flowLayoutPanel1.Controls.Add(this.buttonDefaults);
			this.flowLayoutPanel1.Controls.Add(this.buttonCancel);
			this.flowLayoutPanel1.Controls.Add(this.buttonSave);
			this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
			this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 308);
			this.flowLayoutPanel1.Name = "flowLayoutPanel1";
			this.flowLayoutPanel1.Size = new System.Drawing.Size(378, 29);
			this.flowLayoutPanel1.TabIndex = 0;
			// 
			// buttonDefaults
			// 
			this.buttonDefaults.Location = new System.Drawing.Point(286, 3);
			this.buttonDefaults.Name = "buttonDefaults";
			this.buttonDefaults.Size = new System.Drawing.Size(89, 23);
			this.buttonDefaults.TabIndex = 2;
			this.buttonDefaults.Text = "По умолчанию";
			this.buttonDefaults.UseVisualStyleBackColor = true;
			this.buttonDefaults.Click += new System.EventHandler(this.buttonDefaults_Click);
			// 
			// buttonCancel
			// 
			this.buttonCancel.Location = new System.Drawing.Point(205, 3);
			this.buttonCancel.Name = "buttonCancel";
			this.buttonCancel.Size = new System.Drawing.Size(75, 23);
			this.buttonCancel.TabIndex = 1;
			this.buttonCancel.Text = "Отмена";
			this.buttonCancel.UseVisualStyleBackColor = true;
			this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
			// 
			// buttonSave
			// 
			this.buttonSave.Location = new System.Drawing.Point(122, 3);
			this.buttonSave.Name = "buttonSave";
			this.buttonSave.Size = new System.Drawing.Size(77, 23);
			this.buttonSave.TabIndex = 0;
			this.buttonSave.Text = "ОК";
			this.buttonSave.UseVisualStyleBackColor = true;
			this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
			// 
			// flowLayoutPanel2
			// 
			this.flowLayoutPanel2.AutoScroll = true;
			this.flowLayoutPanel2.AutoSize = true;
			this.flowLayoutPanel2.Controls.Add(this.groupBox1);
			this.flowLayoutPanel2.Controls.Add(this.groupBox2);
			this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.flowLayoutPanel2.Location = new System.Drawing.Point(3, 3);
			this.flowLayoutPanel2.Name = "flowLayoutPanel2";
			this.flowLayoutPanel2.Size = new System.Drawing.Size(378, 299);
			this.flowLayoutPanel2.TabIndex = 1;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.flowLayoutPanel3);
			this.groupBox1.Location = new System.Drawing.Point(3, 3);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(354, 110);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Вывод ошибок";
			// 
			// flowLayoutPanel3
			// 
			this.flowLayoutPanel3.Controls.Add(this.flowLayoutPanel5);
			this.flowLayoutPanel3.Controls.Add(this.flowLayoutPanel6);
			this.flowLayoutPanel3.Controls.Add(this.flowLayoutPanel7);
			this.flowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.flowLayoutPanel3.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
			this.flowLayoutPanel3.Location = new System.Drawing.Point(3, 16);
			this.flowLayoutPanel3.Name = "flowLayoutPanel3";
			this.flowLayoutPanel3.Size = new System.Drawing.Size(348, 91);
			this.flowLayoutPanel3.TabIndex = 0;
			// 
			// flowLayoutPanel5
			// 
			this.flowLayoutPanel5.AutoSize = true;
			this.flowLayoutPanel5.Controls.Add(this.checkShowLineNumbers);
			this.flowLayoutPanel5.Location = new System.Drawing.Point(3, 3);
			this.flowLayoutPanel5.Name = "flowLayoutPanel5";
			this.flowLayoutPanel5.Size = new System.Drawing.Size(168, 23);
			this.flowLayoutPanel5.TabIndex = 0;
			// 
			// checkShowLineNumbers
			// 
			this.checkShowLineNumbers.AutoSize = true;
			this.checkShowLineNumbers.Location = new System.Drawing.Point(3, 3);
			this.checkShowLineNumbers.Name = "checkShowLineNumbers";
			this.checkShowLineNumbers.Size = new System.Drawing.Size(162, 17);
			this.checkShowLineNumbers.TabIndex = 0;
			this.checkShowLineNumbers.Text = "Показывать номер строки";
			this.checkShowLineNumbers.UseVisualStyleBackColor = true;
			// 
			// flowLayoutPanel6
			// 
			this.flowLayoutPanel6.AutoSize = true;
			this.flowLayoutPanel6.Controls.Add(this.checkShowSymbols);
			this.flowLayoutPanel6.Location = new System.Drawing.Point(3, 32);
			this.flowLayoutPanel6.Name = "flowLayoutPanel6";
			this.flowLayoutPanel6.Size = new System.Drawing.Size(191, 23);
			this.flowLayoutPanel6.TabIndex = 1;
			// 
			// checkShowSymbols
			// 
			this.checkShowSymbols.AutoSize = true;
			this.checkShowSymbols.Location = new System.Drawing.Point(3, 3);
			this.checkShowSymbols.Name = "checkShowSymbols";
			this.checkShowSymbols.Size = new System.Drawing.Size(185, 17);
			this.checkShowSymbols.TabIndex = 0;
			this.checkShowSymbols.Text = "Показывать начало параграфа";
			this.checkShowSymbols.UseVisualStyleBackColor = true;
			this.checkShowSymbols.CheckedChanged += new System.EventHandler(this.checkShowSymbols_CheckedChanged);
			// 
			// flowLayoutPanel7
			// 
			this.flowLayoutPanel7.AutoSize = true;
			this.flowLayoutPanel7.Controls.Add(this.labelNNumbers);
			this.flowLayoutPanel7.Controls.Add(this.numericShowNSymbols);
			this.flowLayoutPanel7.Location = new System.Drawing.Point(3, 61);
			this.flowLayoutPanel7.Name = "flowLayoutPanel7";
			this.flowLayoutPanel7.Size = new System.Drawing.Size(242, 26);
			this.flowLayoutPanel7.TabIndex = 2;
			// 
			// labelNNumbers
			// 
			this.labelNNumbers.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.labelNNumbers.AutoSize = true;
			this.labelNNumbers.Location = new System.Drawing.Point(3, 6);
			this.labelNNumbers.Name = "labelNNumbers";
			this.labelNNumbers.Size = new System.Drawing.Size(181, 13);
			this.labelNNumbers.TabIndex = 2;
			this.labelNNumbers.Text = "Количество выводимых символов";
			// 
			// numericShowNSymbols
			// 
			this.numericShowNSymbols.Location = new System.Drawing.Point(190, 3);
			this.numericShowNSymbols.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
			this.numericShowNSymbols.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.numericShowNSymbols.Name = "numericShowNSymbols";
			this.numericShowNSymbols.Size = new System.Drawing.Size(49, 20);
			this.numericShowNSymbols.TabIndex = 1;
			this.numericShowNSymbols.Tag = "";
			this.numericShowNSymbols.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// groupBox2
			// 
			this.groupBox2.AutoSize = true;
			this.groupBox2.Controls.Add(this.flowLayoutPanel4);
			this.groupBox2.Location = new System.Drawing.Point(3, 119);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(354, 787);
			this.groupBox2.TabIndex = 1;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Сообщения о проверке";
			// 
			// flowLayoutPanel4
			// 
			this.flowLayoutPanel4.AutoSize = true;
			this.flowLayoutPanel4.Controls.Add(this.flowLayoutPanel8);
			this.flowLayoutPanel4.Controls.Add(this.flowLayoutPanel9);
			this.flowLayoutPanel4.Controls.Add(this.flowLayoutPanel10);
			this.flowLayoutPanel4.Controls.Add(this.flowLayoutPanel11);
			this.flowLayoutPanel4.Controls.Add(this.flowLayoutPanel12);
			this.flowLayoutPanel4.Controls.Add(this.flowLayoutPanel13);
			this.flowLayoutPanel4.Controls.Add(this.flowLayoutPanel14);
			this.flowLayoutPanel4.Controls.Add(this.flowLayoutPanel15);
			this.flowLayoutPanel4.Controls.Add(this.flowLayoutPanel16);
			this.flowLayoutPanel4.Controls.Add(this.flowLayoutPanel17);
			this.flowLayoutPanel4.Controls.Add(this.flowLayoutPanel18);
			this.flowLayoutPanel4.Controls.Add(this.flowLayoutPanel19);
			this.flowLayoutPanel4.Controls.Add(this.flowLayoutPanel20);
			this.flowLayoutPanel4.Controls.Add(this.flowLayoutPanel21);
			this.flowLayoutPanel4.Controls.Add(this.flowLayoutPanel22);
			this.flowLayoutPanel4.Controls.Add(this.flowLayoutPanel23);
			this.flowLayoutPanel4.Controls.Add(this.flowLayoutPanel24);
			this.flowLayoutPanel4.Controls.Add(this.flowLayoutPanel25);
			this.flowLayoutPanel4.Controls.Add(this.flowLayoutPanel26);
			this.flowLayoutPanel4.Controls.Add(this.flowLayoutPanel27);
			this.flowLayoutPanel4.Controls.Add(this.flowLayoutPanel28);
			this.flowLayoutPanel4.Controls.Add(this.flowLayoutPanel29);
			this.flowLayoutPanel4.Controls.Add(this.flowLayoutPanel30);
			this.flowLayoutPanel4.Controls.Add(this.flowLayoutPanel31);
			this.flowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.flowLayoutPanel4.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
			this.flowLayoutPanel4.Location = new System.Drawing.Point(3, 16);
			this.flowLayoutPanel4.Name = "flowLayoutPanel4";
			this.flowLayoutPanel4.Size = new System.Drawing.Size(348, 768);
			this.flowLayoutPanel4.TabIndex = 0;
			// 
			// flowLayoutPanel8
			// 
			this.flowLayoutPanel8.AutoSize = true;
			this.flowLayoutPanel8.Controls.Add(this.label1);
			this.flowLayoutPanel8.Controls.Add(this.LeftMargin);
			this.flowLayoutPanel8.Location = new System.Drawing.Point(3, 3);
			this.flowLayoutPanel8.Name = "flowLayoutPanel8";
			this.flowLayoutPanel8.Size = new System.Drawing.Size(342, 26);
			this.flowLayoutPanel8.TabIndex = 3;
			// 
			// label1
			// 
			this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(3, 6);
			this.label1.Margin = new System.Windows.Forms.Padding(3, 0, 103, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(75, 13);
			this.label1.TabIndex = 2;
			this.label1.Text = "Отступ слева";
			// 
			// LeftMargin
			// 
			this.LeftMargin.Location = new System.Drawing.Point(184, 3);
			this.LeftMargin.Name = "LeftMargin";
			this.LeftMargin.Size = new System.Drawing.Size(155, 20);
			this.LeftMargin.TabIndex = 3;
			// 
			// flowLayoutPanel9
			// 
			this.flowLayoutPanel9.AutoSize = true;
			this.flowLayoutPanel9.Controls.Add(this.label2);
			this.flowLayoutPanel9.Controls.Add(this.RightMargin);
			this.flowLayoutPanel9.Location = new System.Drawing.Point(3, 35);
			this.flowLayoutPanel9.Name = "flowLayoutPanel9";
			this.flowLayoutPanel9.Size = new System.Drawing.Size(342, 26);
			this.flowLayoutPanel9.TabIndex = 4;
			// 
			// label2
			// 
			this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(3, 6);
			this.label2.Margin = new System.Windows.Forms.Padding(3, 0, 97, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(81, 13);
			this.label2.TabIndex = 2;
			this.label2.Text = "Отступ справа";
			// 
			// RightMargin
			// 
			this.RightMargin.Location = new System.Drawing.Point(184, 3);
			this.RightMargin.Name = "RightMargin";
			this.RightMargin.Size = new System.Drawing.Size(155, 20);
			this.RightMargin.TabIndex = 3;
			// 
			// flowLayoutPanel10
			// 
			this.flowLayoutPanel10.AutoSize = true;
			this.flowLayoutPanel10.Controls.Add(this.label3);
			this.flowLayoutPanel10.Controls.Add(this.BottomMargin);
			this.flowLayoutPanel10.Location = new System.Drawing.Point(3, 67);
			this.flowLayoutPanel10.Name = "flowLayoutPanel10";
			this.flowLayoutPanel10.Size = new System.Drawing.Size(342, 26);
			this.flowLayoutPanel10.TabIndex = 5;
			// 
			// label3
			// 
			this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(3, 6);
			this.label3.Margin = new System.Windows.Forms.Padding(3, 0, 104, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(74, 13);
			this.label3.TabIndex = 2;
			this.label3.Text = "Отступ снизу";
			// 
			// BottomMargin
			// 
			this.BottomMargin.Location = new System.Drawing.Point(184, 3);
			this.BottomMargin.Name = "BottomMargin";
			this.BottomMargin.Size = new System.Drawing.Size(155, 20);
			this.BottomMargin.TabIndex = 3;
			// 
			// flowLayoutPanel11
			// 
			this.flowLayoutPanel11.AutoSize = true;
			this.flowLayoutPanel11.Controls.Add(this.label4);
			this.flowLayoutPanel11.Controls.Add(this.TopMargin);
			this.flowLayoutPanel11.Location = new System.Drawing.Point(3, 99);
			this.flowLayoutPanel11.Name = "flowLayoutPanel11";
			this.flowLayoutPanel11.Size = new System.Drawing.Size(342, 26);
			this.flowLayoutPanel11.TabIndex = 6;
			// 
			// label4
			// 
			this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(3, 6);
			this.label4.Margin = new System.Windows.Forms.Padding(3, 0, 99, 0);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(79, 13);
			this.label4.TabIndex = 2;
			this.label4.Text = "Отступ сверху";
			// 
			// TopMargin
			// 
			this.TopMargin.Location = new System.Drawing.Point(184, 3);
			this.TopMargin.Name = "TopMargin";
			this.TopMargin.Size = new System.Drawing.Size(155, 20);
			this.TopMargin.TabIndex = 3;
			// 
			// flowLayoutPanel12
			// 
			this.flowLayoutPanel12.AutoSize = true;
			this.flowLayoutPanel12.Controls.Add(this.label5);
			this.flowLayoutPanel12.Controls.Add(this.PageOrientation);
			this.flowLayoutPanel12.Location = new System.Drawing.Point(3, 131);
			this.flowLayoutPanel12.Name = "flowLayoutPanel12";
			this.flowLayoutPanel12.Size = new System.Drawing.Size(342, 26);
			this.flowLayoutPanel12.TabIndex = 7;
			// 
			// label5
			// 
			this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(3, 6);
			this.label5.Margin = new System.Windows.Forms.Padding(3, 0, 58, 0);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(120, 13);
			this.label5.TabIndex = 2;
			this.label5.Text = "Ориентация страницы";
			// 
			// PageOrientation
			// 
			this.PageOrientation.Location = new System.Drawing.Point(184, 3);
			this.PageOrientation.Name = "PageOrientation";
			this.PageOrientation.Size = new System.Drawing.Size(155, 20);
			this.PageOrientation.TabIndex = 3;
			// 
			// flowLayoutPanel13
			// 
			this.flowLayoutPanel13.AutoSize = true;
			this.flowLayoutPanel13.Controls.Add(this.label6);
			this.flowLayoutPanel13.Controls.Add(this.PaperSize);
			this.flowLayoutPanel13.Location = new System.Drawing.Point(3, 163);
			this.flowLayoutPanel13.Name = "flowLayoutPanel13";
			this.flowLayoutPanel13.Size = new System.Drawing.Size(342, 26);
			this.flowLayoutPanel13.TabIndex = 8;
			// 
			// label6
			// 
			this.label6.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(3, 6);
			this.label6.Margin = new System.Windows.Forms.Padding(3, 0, 80, 0);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(98, 13);
			this.label6.TabIndex = 2;
			this.label6.Text = "Размер страницы";
			// 
			// PaperSize
			// 
			this.PaperSize.Location = new System.Drawing.Point(184, 3);
			this.PaperSize.Name = "PaperSize";
			this.PaperSize.Size = new System.Drawing.Size(155, 20);
			this.PaperSize.TabIndex = 3;
			// 
			// flowLayoutPanel14
			// 
			this.flowLayoutPanel14.AutoSize = true;
			this.flowLayoutPanel14.Controls.Add(this.label7);
			this.flowLayoutPanel14.Controls.Add(this.ParagraphAlign);
			this.flowLayoutPanel14.Location = new System.Drawing.Point(3, 195);
			this.flowLayoutPanel14.Name = "flowLayoutPanel14";
			this.flowLayoutPanel14.Size = new System.Drawing.Size(342, 26);
			this.flowLayoutPanel14.TabIndex = 9;
			// 
			// label7
			// 
			this.label7.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(3, 6);
			this.label7.Margin = new System.Windows.Forms.Padding(3, 0, 38, 0);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(140, 13);
			this.label7.TabIndex = 2;
			this.label7.Text = "Выравнивание параграфа";
			// 
			// ParagraphAlign
			// 
			this.ParagraphAlign.Location = new System.Drawing.Point(184, 3);
			this.ParagraphAlign.Name = "ParagraphAlign";
			this.ParagraphAlign.Size = new System.Drawing.Size(155, 20);
			this.ParagraphAlign.TabIndex = 3;
			// 
			// flowLayoutPanel15
			// 
			this.flowLayoutPanel15.AutoSize = true;
			this.flowLayoutPanel15.Controls.Add(this.label8);
			this.flowLayoutPanel15.Controls.Add(this.ParagraphSpacing);
			this.flowLayoutPanel15.Location = new System.Drawing.Point(3, 227);
			this.flowLayoutPanel15.Name = "flowLayoutPanel15";
			this.flowLayoutPanel15.Size = new System.Drawing.Size(342, 26);
			this.flowLayoutPanel15.TabIndex = 10;
			// 
			// label8
			// 
			this.label8.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(3, 6);
			this.label8.Margin = new System.Windows.Forms.Padding(3, 0, 39, 0);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(139, 13);
			this.label8.TabIndex = 2;
			this.label8.Text = "Междустрочный интервал";
			// 
			// ParagraphSpacing
			// 
			this.ParagraphSpacing.Location = new System.Drawing.Point(184, 3);
			this.ParagraphSpacing.Name = "ParagraphSpacing";
			this.ParagraphSpacing.Size = new System.Drawing.Size(155, 20);
			this.ParagraphSpacing.TabIndex = 3;
			// 
			// flowLayoutPanel16
			// 
			this.flowLayoutPanel16.AutoSize = true;
			this.flowLayoutPanel16.Controls.Add(this.label9);
			this.flowLayoutPanel16.Controls.Add(this.ParagraphIndent);
			this.flowLayoutPanel16.Location = new System.Drawing.Point(3, 259);
			this.flowLayoutPanel16.Name = "flowLayoutPanel16";
			this.flowLayoutPanel16.Size = new System.Drawing.Size(342, 26);
			this.flowLayoutPanel16.TabIndex = 11;
			// 
			// label9
			// 
			this.label9.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(3, 6);
			this.label9.Margin = new System.Windows.Forms.Padding(3, 0, 59, 0);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(119, 13);
			this.label9.TabIndex = 2;
			this.label9.Text = "Отступ первой строки";
			// 
			// ParagraphIndent
			// 
			this.ParagraphIndent.Location = new System.Drawing.Point(184, 3);
			this.ParagraphIndent.Name = "ParagraphIndent";
			this.ParagraphIndent.Size = new System.Drawing.Size(155, 20);
			this.ParagraphIndent.TabIndex = 3;
			// 
			// flowLayoutPanel17
			// 
			this.flowLayoutPanel17.AutoSize = true;
			this.flowLayoutPanel17.Controls.Add(this.label10);
			this.flowLayoutPanel17.Controls.Add(this.ParagraphHangingLines);
			this.flowLayoutPanel17.Location = new System.Drawing.Point(3, 291);
			this.flowLayoutPanel17.Name = "flowLayoutPanel17";
			this.flowLayoutPanel17.Size = new System.Drawing.Size(342, 26);
			this.flowLayoutPanel17.TabIndex = 12;
			// 
			// label10
			// 
			this.label10.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(3, 6);
			this.label10.Margin = new System.Windows.Forms.Padding(3, 0, 60, 0);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(118, 13);
			this.label10.TabIndex = 2;
			this.label10.Text = "Запрет висячих строк";
			// 
			// ParagraphHangingLines
			// 
			this.ParagraphHangingLines.Location = new System.Drawing.Point(184, 3);
			this.ParagraphHangingLines.Name = "ParagraphHangingLines";
			this.ParagraphHangingLines.Size = new System.Drawing.Size(155, 20);
			this.ParagraphHangingLines.TabIndex = 3;
			// 
			// flowLayoutPanel18
			// 
			this.flowLayoutPanel18.AutoSize = true;
			this.flowLayoutPanel18.Controls.Add(this.label11);
			this.flowLayoutPanel18.Controls.Add(this.NumberedListLevel1);
			this.flowLayoutPanel18.Location = new System.Drawing.Point(3, 323);
			this.flowLayoutPanel18.Name = "flowLayoutPanel18";
			this.flowLayoutPanel18.Size = new System.Drawing.Size(342, 26);
			this.flowLayoutPanel18.TabIndex = 13;
			// 
			// label11
			// 
			this.label11.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(3, 6);
			this.label11.Margin = new System.Windows.Forms.Padding(3, 0, 47, 0);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(131, 13);
			this.label11.TabIndex = 2;
			this.label11.Text = "Нум. список 1-го уровня";
			// 
			// NumberedListLevel1
			// 
			this.NumberedListLevel1.Location = new System.Drawing.Point(184, 3);
			this.NumberedListLevel1.Name = "NumberedListLevel1";
			this.NumberedListLevel1.Size = new System.Drawing.Size(155, 20);
			this.NumberedListLevel1.TabIndex = 3;
			// 
			// flowLayoutPanel19
			// 
			this.flowLayoutPanel19.AutoSize = true;
			this.flowLayoutPanel19.Controls.Add(this.label12);
			this.flowLayoutPanel19.Controls.Add(this.NumberedListLevel2);
			this.flowLayoutPanel19.Location = new System.Drawing.Point(3, 355);
			this.flowLayoutPanel19.Name = "flowLayoutPanel19";
			this.flowLayoutPanel19.Size = new System.Drawing.Size(342, 26);
			this.flowLayoutPanel19.TabIndex = 14;
			// 
			// label12
			// 
			this.label12.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(3, 6);
			this.label12.Margin = new System.Windows.Forms.Padding(3, 0, 47, 0);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(131, 13);
			this.label12.TabIndex = 2;
			this.label12.Text = "Нум. список 2-го уровня";
			// 
			// NumberedListLevel2
			// 
			this.NumberedListLevel2.Location = new System.Drawing.Point(184, 3);
			this.NumberedListLevel2.Name = "NumberedListLevel2";
			this.NumberedListLevel2.Size = new System.Drawing.Size(155, 20);
			this.NumberedListLevel2.TabIndex = 3;
			// 
			// flowLayoutPanel20
			// 
			this.flowLayoutPanel20.AutoSize = true;
			this.flowLayoutPanel20.Controls.Add(this.label13);
			this.flowLayoutPanel20.Controls.Add(this.BulletedList);
			this.flowLayoutPanel20.Location = new System.Drawing.Point(3, 387);
			this.flowLayoutPanel20.Name = "flowLayoutPanel20";
			this.flowLayoutPanel20.Size = new System.Drawing.Size(342, 26);
			this.flowLayoutPanel20.TabIndex = 15;
			// 
			// label13
			// 
			this.label13.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label13.AutoSize = true;
			this.label13.Location = new System.Drawing.Point(3, 6);
			this.label13.Margin = new System.Windows.Forms.Padding(3, 0, 49, 0);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(129, 13);
			this.label13.TabIndex = 2;
			this.label13.Text = "Маркированный список";
			// 
			// BulletedList
			// 
			this.BulletedList.Location = new System.Drawing.Point(184, 3);
			this.BulletedList.Name = "BulletedList";
			this.BulletedList.Size = new System.Drawing.Size(155, 20);
			this.BulletedList.TabIndex = 3;
			// 
			// flowLayoutPanel21
			// 
			this.flowLayoutPanel21.AutoSize = true;
			this.flowLayoutPanel21.Controls.Add(this.label14);
			this.flowLayoutPanel21.Controls.Add(this.FontFont);
			this.flowLayoutPanel21.Location = new System.Drawing.Point(3, 419);
			this.flowLayoutPanel21.Name = "flowLayoutPanel21";
			this.flowLayoutPanel21.Size = new System.Drawing.Size(342, 26);
			this.flowLayoutPanel21.TabIndex = 16;
			// 
			// label14
			// 
			this.label14.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label14.AutoSize = true;
			this.label14.Location = new System.Drawing.Point(3, 6);
			this.label14.Margin = new System.Windows.Forms.Padding(3, 0, 137, 0);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(41, 13);
			this.label14.TabIndex = 2;
			this.label14.Text = "Шрифт";
			// 
			// FontFont
			// 
			this.FontFont.Location = new System.Drawing.Point(184, 3);
			this.FontFont.Name = "FontFont";
			this.FontFont.Size = new System.Drawing.Size(155, 20);
			this.FontFont.TabIndex = 3;
			// 
			// flowLayoutPanel22
			// 
			this.flowLayoutPanel22.AutoSize = true;
			this.flowLayoutPanel22.Controls.Add(this.label15);
			this.flowLayoutPanel22.Controls.Add(this.FontSize);
			this.flowLayoutPanel22.Location = new System.Drawing.Point(3, 451);
			this.flowLayoutPanel22.Name = "flowLayoutPanel22";
			this.flowLayoutPanel22.Size = new System.Drawing.Size(342, 26);
			this.flowLayoutPanel22.TabIndex = 17;
			// 
			// label15
			// 
			this.label15.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label15.AutoSize = true;
			this.label15.Location = new System.Drawing.Point(3, 6);
			this.label15.Margin = new System.Windows.Forms.Padding(3, 0, 32, 0);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(146, 13);
			this.label15.TabIndex = 2;
			this.label15.Text = "Проверка размера шрифта";
			// 
			// FontSize
			// 
			this.FontSize.Location = new System.Drawing.Point(184, 3);
			this.FontSize.Name = "FontSize";
			this.FontSize.Size = new System.Drawing.Size(155, 20);
			this.FontSize.TabIndex = 3;
			// 
			// flowLayoutPanel23
			// 
			this.flowLayoutPanel23.AutoSize = true;
			this.flowLayoutPanel23.Controls.Add(this.label16);
			this.flowLayoutPanel23.Controls.Add(this.FontBlack);
			this.flowLayoutPanel23.Location = new System.Drawing.Point(3, 483);
			this.flowLayoutPanel23.Name = "flowLayoutPanel23";
			this.flowLayoutPanel23.Size = new System.Drawing.Size(342, 26);
			this.flowLayoutPanel23.TabIndex = 18;
			// 
			// label16
			// 
			this.label16.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label16.AutoSize = true;
			this.label16.Location = new System.Drawing.Point(3, 6);
			this.label16.Margin = new System.Windows.Forms.Padding(3, 0, 63, 0);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(115, 13);
			this.label16.TabIndex = 2;
			this.label16.Text = "Только чёрный текст";
			// 
			// FontBlack
			// 
			this.FontBlack.Location = new System.Drawing.Point(184, 3);
			this.FontBlack.Name = "FontBlack";
			this.FontBlack.Size = new System.Drawing.Size(155, 20);
			this.FontBlack.TabIndex = 3;
			// 
			// flowLayoutPanel24
			// 
			this.flowLayoutPanel24.AutoSize = true;
			this.flowLayoutPanel24.Controls.Add(this.label17);
			this.flowLayoutPanel24.Controls.Add(this.FontNotStylized);
			this.flowLayoutPanel24.Location = new System.Drawing.Point(3, 515);
			this.flowLayoutPanel24.Name = "flowLayoutPanel24";
			this.flowLayoutPanel24.Size = new System.Drawing.Size(342, 26);
			this.flowLayoutPanel24.TabIndex = 19;
			// 
			// label17
			// 
			this.label17.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label17.AutoSize = true;
			this.label17.Location = new System.Drawing.Point(3, 6);
			this.label17.Margin = new System.Windows.Forms.Padding(3, 0, 114, 0);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(64, 13);
			this.label17.TabIndex = 2;
			this.label17.Text = "Без стилей";
			// 
			// FontNotStylized
			// 
			this.FontNotStylized.Location = new System.Drawing.Point(184, 3);
			this.FontNotStylized.Name = "FontNotStylized";
			this.FontNotStylized.Size = new System.Drawing.Size(155, 20);
			this.FontNotStylized.TabIndex = 3;
			// 
			// flowLayoutPanel25
			// 
			this.flowLayoutPanel25.AutoSize = true;
			this.flowLayoutPanel25.Controls.Add(this.label18);
			this.flowLayoutPanel25.Controls.Add(this.IsNumerated);
			this.flowLayoutPanel25.Location = new System.Drawing.Point(3, 547);
			this.flowLayoutPanel25.Name = "flowLayoutPanel25";
			this.flowLayoutPanel25.Size = new System.Drawing.Size(342, 26);
			this.flowLayoutPanel25.TabIndex = 20;
			// 
			// label18
			// 
			this.label18.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label18.AutoSize = true;
			this.label18.Location = new System.Drawing.Point(3, 6);
			this.label18.Margin = new System.Windows.Forms.Padding(3, 0, 70, 0);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(108, 13);
			this.label18.TabIndex = 2;
			this.label18.Text = "Наличие нумерации";
			// 
			// IsNumerated
			// 
			this.IsNumerated.Location = new System.Drawing.Point(184, 3);
			this.IsNumerated.Name = "IsNumerated";
			this.IsNumerated.Size = new System.Drawing.Size(155, 20);
			this.IsNumerated.TabIndex = 3;
			// 
			// flowLayoutPanel26
			// 
			this.flowLayoutPanel26.AutoSize = true;
			this.flowLayoutPanel26.Controls.Add(this.label19);
			this.flowLayoutPanel26.Controls.Add(this.IsNotRestarted);
			this.flowLayoutPanel26.Location = new System.Drawing.Point(3, 579);
			this.flowLayoutPanel26.Name = "flowLayoutPanel26";
			this.flowLayoutPanel26.Size = new System.Drawing.Size(342, 26);
			this.flowLayoutPanel26.TabIndex = 21;
			// 
			// label19
			// 
			this.label19.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label19.AutoSize = true;
			this.label19.Location = new System.Drawing.Point(3, 6);
			this.label19.Margin = new System.Windows.Forms.Padding(3, 0, 22, 0);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(156, 13);
			this.label19.TabIndex = 2;
			this.label19.Text = "Не сбрасывается в разделах";
			// 
			// IsNotRestarted
			// 
			this.IsNotRestarted.Location = new System.Drawing.Point(184, 3);
			this.IsNotRestarted.Name = "IsNotRestarted";
			this.IsNotRestarted.Size = new System.Drawing.Size(155, 20);
			this.IsNotRestarted.TabIndex = 3;
			// 
			// flowLayoutPanel27
			// 
			this.flowLayoutPanel27.AutoSize = true;
			this.flowLayoutPanel27.Controls.Add(this.label20);
			this.flowLayoutPanel27.Controls.Add(this.NoNumerationTop);
			this.flowLayoutPanel27.Location = new System.Drawing.Point(3, 611);
			this.flowLayoutPanel27.Name = "flowLayoutPanel27";
			this.flowLayoutPanel27.Size = new System.Drawing.Size(342, 26);
			this.flowLayoutPanel27.TabIndex = 22;
			// 
			// label20
			// 
			this.label20.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label20.AutoSize = true;
			this.label20.Location = new System.Drawing.Point(3, 6);
			this.label20.Margin = new System.Windows.Forms.Padding(3, 0, 57, 0);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(121, 13);
			this.label20.TabIndex = 2;
			this.label20.Text = "Нет нумерации сверху";
			// 
			// NoNumerationTop
			// 
			this.NoNumerationTop.Location = new System.Drawing.Point(184, 3);
			this.NoNumerationTop.Name = "NoNumerationTop";
			this.NoNumerationTop.Size = new System.Drawing.Size(155, 20);
			this.NoNumerationTop.TabIndex = 3;
			// 
			// flowLayoutPanel28
			// 
			this.flowLayoutPanel28.AutoSize = true;
			this.flowLayoutPanel28.Controls.Add(this.label21);
			this.flowLayoutPanel28.Controls.Add(this.NumerationStyle);
			this.flowLayoutPanel28.Location = new System.Drawing.Point(3, 643);
			this.flowLayoutPanel28.Name = "flowLayoutPanel28";
			this.flowLayoutPanel28.Size = new System.Drawing.Size(342, 26);
			this.flowLayoutPanel28.TabIndex = 23;
			// 
			// label21
			// 
			this.label21.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label21.AutoSize = true;
			this.label21.Location = new System.Drawing.Point(3, 6);
			this.label21.Margin = new System.Windows.Forms.Padding(3, 0, 83, 0);
			this.label21.Name = "label21";
			this.label21.Size = new System.Drawing.Size(95, 13);
			this.label21.TabIndex = 2;
			this.label21.Text = "Стиль нумерации";
			// 
			// NumerationStyle
			// 
			this.NumerationStyle.Location = new System.Drawing.Point(184, 3);
			this.NumerationStyle.Name = "NumerationStyle";
			this.NumerationStyle.Size = new System.Drawing.Size(155, 20);
			this.NumerationStyle.TabIndex = 3;
			// 
			// flowLayoutPanel29
			// 
			this.flowLayoutPanel29.AutoSize = true;
			this.flowLayoutPanel29.Controls.Add(this.label22);
			this.flowLayoutPanel29.Controls.Add(this.NumerationAlign);
			this.flowLayoutPanel29.Location = new System.Drawing.Point(3, 675);
			this.flowLayoutPanel29.Name = "flowLayoutPanel29";
			this.flowLayoutPanel29.Size = new System.Drawing.Size(342, 26);
			this.flowLayoutPanel29.TabIndex = 24;
			// 
			// label22
			// 
			this.label22.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label22.AutoSize = true;
			this.label22.Location = new System.Drawing.Point(3, 6);
			this.label22.Margin = new System.Windows.Forms.Padding(3, 0, 38, 0);
			this.label22.Name = "label22";
			this.label22.Size = new System.Drawing.Size(140, 13);
			this.label22.TabIndex = 2;
			this.label22.Text = "Выравнивание нумерации";
			// 
			// NumerationAlign
			// 
			this.NumerationAlign.Location = new System.Drawing.Point(184, 3);
			this.NumerationAlign.Name = "NumerationAlign";
			this.NumerationAlign.Size = new System.Drawing.Size(155, 20);
			this.NumerationAlign.TabIndex = 3;
			// 
			// flowLayoutPanel30
			// 
			this.flowLayoutPanel30.AutoSize = true;
			this.flowLayoutPanel30.Controls.Add(this.label23);
			this.flowLayoutPanel30.Controls.Add(this.DifferentFirstPage);
			this.flowLayoutPanel30.Location = new System.Drawing.Point(3, 707);
			this.flowLayoutPanel30.Name = "flowLayoutPanel30";
			this.flowLayoutPanel30.Size = new System.Drawing.Size(342, 26);
			this.flowLayoutPanel30.TabIndex = 25;
			// 
			// label23
			// 
			this.label23.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label23.AutoSize = true;
			this.label23.Location = new System.Drawing.Point(3, 6);
			this.label23.Margin = new System.Windows.Forms.Padding(3, 0, 21, 0);
			this.label23.Name = "label23";
			this.label23.Size = new System.Drawing.Size(157, 13);
			this.label23.TabIndex = 2;
			this.label23.Text = "Первая страница без номера";
			// 
			// DifferentFirstPage
			// 
			this.DifferentFirstPage.Location = new System.Drawing.Point(184, 3);
			this.DifferentFirstPage.Name = "DifferentFirstPage";
			this.DifferentFirstPage.Size = new System.Drawing.Size(155, 20);
			this.DifferentFirstPage.TabIndex = 3;
			// 
			// flowLayoutPanel31
			// 
			this.flowLayoutPanel31.AutoSize = true;
			this.flowLayoutPanel31.Controls.Add(this.label24);
			this.flowLayoutPanel31.Controls.Add(this.DifferentLastPage);
			this.flowLayoutPanel31.Location = new System.Drawing.Point(3, 739);
			this.flowLayoutPanel31.Name = "flowLayoutPanel31";
			this.flowLayoutPanel31.Size = new System.Drawing.Size(342, 26);
			this.flowLayoutPanel31.TabIndex = 26;
			// 
			// label24
			// 
			this.label24.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label24.AutoSize = true;
			this.label24.Location = new System.Drawing.Point(3, 6);
			this.label24.Name = "label24";
			this.label24.Size = new System.Drawing.Size(175, 13);
			this.label24.TabIndex = 2;
			this.label24.Text = "Последняя страница без номера";
			// 
			// DifferentLastPage
			// 
			this.DifferentLastPage.Location = new System.Drawing.Point(184, 3);
			this.DifferentLastPage.Name = "DifferentLastPage";
			this.DifferentLastPage.Size = new System.Drawing.Size(155, 20);
			this.DifferentLastPage.TabIndex = 3;
			// 
			// FormSettings
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(384, 340);
			this.Controls.Add(this.tableLayoutPanel1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximumSize = new System.Drawing.Size(400, 2000);
			this.MinimumSize = new System.Drawing.Size(400, 38);
			this.Name = "FormSettings";
			this.Text = "Параметры";
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			this.flowLayoutPanel1.ResumeLayout(false);
			this.flowLayoutPanel2.ResumeLayout(false);
			this.flowLayoutPanel2.PerformLayout();
			this.groupBox1.ResumeLayout(false);
			this.flowLayoutPanel3.ResumeLayout(false);
			this.flowLayoutPanel3.PerformLayout();
			this.flowLayoutPanel5.ResumeLayout(false);
			this.flowLayoutPanel5.PerformLayout();
			this.flowLayoutPanel6.ResumeLayout(false);
			this.flowLayoutPanel6.PerformLayout();
			this.flowLayoutPanel7.ResumeLayout(false);
			this.flowLayoutPanel7.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericShowNSymbols)).EndInit();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.flowLayoutPanel4.ResumeLayout(false);
			this.flowLayoutPanel4.PerformLayout();
			this.flowLayoutPanel8.ResumeLayout(false);
			this.flowLayoutPanel8.PerformLayout();
			this.flowLayoutPanel9.ResumeLayout(false);
			this.flowLayoutPanel9.PerformLayout();
			this.flowLayoutPanel10.ResumeLayout(false);
			this.flowLayoutPanel10.PerformLayout();
			this.flowLayoutPanel11.ResumeLayout(false);
			this.flowLayoutPanel11.PerformLayout();
			this.flowLayoutPanel12.ResumeLayout(false);
			this.flowLayoutPanel12.PerformLayout();
			this.flowLayoutPanel13.ResumeLayout(false);
			this.flowLayoutPanel13.PerformLayout();
			this.flowLayoutPanel14.ResumeLayout(false);
			this.flowLayoutPanel14.PerformLayout();
			this.flowLayoutPanel15.ResumeLayout(false);
			this.flowLayoutPanel15.PerformLayout();
			this.flowLayoutPanel16.ResumeLayout(false);
			this.flowLayoutPanel16.PerformLayout();
			this.flowLayoutPanel17.ResumeLayout(false);
			this.flowLayoutPanel17.PerformLayout();
			this.flowLayoutPanel18.ResumeLayout(false);
			this.flowLayoutPanel18.PerformLayout();
			this.flowLayoutPanel19.ResumeLayout(false);
			this.flowLayoutPanel19.PerformLayout();
			this.flowLayoutPanel20.ResumeLayout(false);
			this.flowLayoutPanel20.PerformLayout();
			this.flowLayoutPanel21.ResumeLayout(false);
			this.flowLayoutPanel21.PerformLayout();
			this.flowLayoutPanel22.ResumeLayout(false);
			this.flowLayoutPanel22.PerformLayout();
			this.flowLayoutPanel23.ResumeLayout(false);
			this.flowLayoutPanel23.PerformLayout();
			this.flowLayoutPanel24.ResumeLayout(false);
			this.flowLayoutPanel24.PerformLayout();
			this.flowLayoutPanel25.ResumeLayout(false);
			this.flowLayoutPanel25.PerformLayout();
			this.flowLayoutPanel26.ResumeLayout(false);
			this.flowLayoutPanel26.PerformLayout();
			this.flowLayoutPanel27.ResumeLayout(false);
			this.flowLayoutPanel27.PerformLayout();
			this.flowLayoutPanel28.ResumeLayout(false);
			this.flowLayoutPanel28.PerformLayout();
			this.flowLayoutPanel29.ResumeLayout(false);
			this.flowLayoutPanel29.PerformLayout();
			this.flowLayoutPanel30.ResumeLayout(false);
			this.flowLayoutPanel30.PerformLayout();
			this.flowLayoutPanel31.ResumeLayout(false);
			this.flowLayoutPanel31.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
		private System.Windows.Forms.Button buttonDefaults;
		private System.Windows.Forms.Button buttonCancel;
		private System.Windows.Forms.Button buttonSave;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
		private System.Windows.Forms.CheckBox checkShowLineNumbers;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
		private System.Windows.Forms.CheckBox checkShowSymbols;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
		private System.Windows.Forms.Label labelNNumbers;
		private System.Windows.Forms.NumericUpDown numericShowNSymbols;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox LeftMargin;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox RightMargin;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel10;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox BottomMargin;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel11;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox TopMargin;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel12;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox PageOrientation;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel13;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox PaperSize;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel14;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox ParagraphAlign;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel15;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox ParagraphSpacing;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel16;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox ParagraphIndent;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel17;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.TextBox ParagraphHangingLines;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel18;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.TextBox NumberedListLevel1;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel19;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.TextBox NumberedListLevel2;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel20;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.TextBox BulletedList;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel21;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.TextBox FontFont;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel22;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.TextBox FontSize;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel23;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.TextBox FontBlack;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel24;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.TextBox FontNotStylized;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel25;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.TextBox IsNumerated;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel26;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.TextBox IsNotRestarted;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel27;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.TextBox NoNumerationTop;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel28;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.TextBox NumerationStyle;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel29;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.TextBox NumerationAlign;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel30;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.TextBox DifferentFirstPage;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel31;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.TextBox DifferentLastPage;
	}
}