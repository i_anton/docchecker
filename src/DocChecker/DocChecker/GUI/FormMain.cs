﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;
using DocChecker.Core;
using DocChecker.Core.Checks;

namespace DocChecker.GUI
{
	public partial class FormMain : Form
	{
		BackgroundWorker backgroundWorker;
		public FormMain()
		{
			_addCheckMenuItems = new Dictionary<Type, ToolStripItem>();

			InitializeComponent();
			ApplicationLog.LogCalled += LogApplicationMessage;
			ApplicationLog.LogTreeCalled += LogTreeMessage;
			ApplicationLog.LogStateCalled += LogStateMessage;
			CheckList.CheckAdded += OnCheckListCheckAdded;
			CheckList.CheckRemoved += OnCheckListCheckRemoved;
			CheckList.CheckListCleared += OnCheckListClear;

			foreach (var checkType in CheckList.GetAllChecksTypes())
			{
				var menuItem = addCheckToolStripMenuItem.DropDownItems.Add(
					((Check) checkType.GetConstructor(Type.EmptyTypes).Invoke(new Object[0])).Name, //Сверхгрязный хак
					null,
					(miSender, miE) => CheckList.Add(checkType));
				_addCheckMenuItems.Add(checkType, menuItem);
			}

			CheckList.Add(typeof(PageFormatCheck));
			CheckList.Add(typeof(ParagraphCheck));
			CheckList.Add(typeof(FontCheck));
			CheckList.Add(typeof(NumerationCheck));
			CheckList.Add(typeof(StyleCheck));
			if (Properties.Settings.Default.LastUserTemplate != "")
			{
				try
				{
					TemplatesOperations.Load(Properties.Settings.Default.LastUserTemplate);
				}
				catch (Exception exception)
				{
					ApplicationLog.LogError("Возникла ошибка при загрузке шаблона "
						+ Properties.Settings.Default.LastUserTemplate
						+ Environment.NewLine
						+ exception.GetType()
						+ exception.Message);
					Properties.Settings.Default.LastUserTemplate = "";
					Properties.Settings.Default.Save();
				}
			}
		}

		private void SaveToolStripMenuItem_Click(Object sender, EventArgs e)
		{
			saveLogDialog.ShowDialog();
		}

		private void saveLogDialog_FileOk(Object sender, CancelEventArgs e)
		{
			File.WriteAllText(saveLogDialog.FileName, LogOutput.Text);
			ApplicationLog.LogMessage("Лог сохранён как " + saveLogDialog.FileName);
		}

		private void ExitToolStripMenuItem_Click(Object sender, EventArgs e)
		{
			Application.Exit();
		}

		private void AboutToolStripMenuItem_Click(Object sender, EventArgs e)
		{
			MessageBox.Show(
				"Программа для проверки документов .DOCX" + Environment.NewLine + Environment.NewLine
				+ "Истомин Антон" + Environment.NewLine
				+ "Пелагейкин Георгий" + Environment.NewLine + Environment.NewLine
				+ "2016"
				);
		}

		private void CheckButton_Click(Object sender, EventArgs e)
		{
			treeView.Nodes.Clear();
			toolStripCheckDoc.Enabled = false;
			toolStripAbort.Enabled = true;
			toolStripOpenDoc.Enabled = false;
			LogParametersToolStripMenuItem.Enabled = false;
			openDocumentToolStripMenuItem.Enabled = false;

			backgroundWorker = new BackgroundWorker { WorkerReportsProgress = true, WorkerSupportsCancellation = true };

			backgroundWorker.DoWork +=
				(bwSender, bwE) =>
					CheckList.InvokeAll(new Tuple<WordDocument, BackgroundWorker>(DocumentControl.CurrentDocument, backgroundWorker));
			backgroundWorker.ProgressChanged += (bwSender, bwE) => checksProgressBar.Value = bwE.ProgressPercentage;
			backgroundWorker.RunWorkerCompleted += (bwSender, bwE) =>
			{
				openDocumentToolStripMenuItem.Enabled = true;
				LogParametersToolStripMenuItem.Enabled = true;
				toolStripOpenDoc.Enabled = true;
				toolStripCheckDoc.Enabled = true;
				toolStripAbort.Enabled = false;
				backgroundWorker.Dispose();
			};
			checksProgressBar.Value = 0;
			backgroundWorker.RunWorkerAsync();
		}

		private void ClearLogButton_Click(Object sender, EventArgs e)
		{
			ThreadSafeLogOutputSetText(String.Empty);
		}


		private void TemplateImportToolStripMenuItem_Click(Object sender, EventArgs e)
		{
			openTemplateDialog.ShowDialog();
		}

		private void TemplateExportStripMenuItem_Click(Object sender, EventArgs e)
		{
			saveTemplateDialog.ShowDialog();
		}

		private void openTemplateDialog_FileOk(Object sender, CancelEventArgs e)
		{
			try
			{
				TemplatesOperations.Load(openTemplateDialog.FileName);
				Properties.Settings.Default.LastUserTemplate = openTemplateDialog.FileName;
				Properties.Settings.Default.Save();
				
			}
			catch (Exception exception)
			{
				ApplicationLog.LogError("Возникла ошибка при загрузке шаблона:" + Environment.NewLine
				                        + exception.GetType() + Environment.NewLine
				                        + exception.Message);
			}
		}

		private void saveTemplateDialog_FileOk(Object sender, CancelEventArgs e)
		{
			try
			{
				TemplatesOperations.Export(saveTemplateDialog.FileName);
			}
			catch (Exception exception)
			{
				ApplicationLog.LogError("Возникла ошибка при сохранении шаблона:"
				                        + Environment.NewLine
				                        + exception.GetType()
				                        + exception.Message);
			}
		}


		private void openDocumentButton_Click(Object sender, EventArgs e)
		{
			openDocumentDialog.ShowDialog();
		}

		private void openDocumentDialog_FileOk(Object sender, CancelEventArgs e)
		{
			ApplicationLog.LogMessage($"Выбран файл: {openDocumentDialog.FileName}");
			ApplicationLog.LogState("Считывание стилей");
			DocumentControl.Open(openDocumentDialog.FileName);
			this.Text = "Проверка форматирования документов: " + openDocumentDialog.FileName;
			toolStripCheckDoc.Enabled = true;
			treeView.Nodes.Clear();
			ApplicationLog.LogState("Готов");
		}

		private void LogStateMessage(String state)
		{
			ThreadSafeCurrentStateSetText(state);
		}
		private void LogTreeMessage(TreeNode RootNode)
		{
			ThreadSafeLogTreeAppendNode(RootNode);
		}

		private void LogApplicationMessage(String message)
		{
			ThreadSafeLogOutputAppendText(message + Environment.NewLine + Environment.NewLine);
		}

		private void ThreadSafeLogTreeAppendNode(TreeNode RootNode)
		{
			if (treeView.InvokeRequired)
			{
				Invoke(new SetNodeCallback(ThreadSafeLogTreeAppendNode), RootNode);
			}
			else
			{
				treeView.BeginUpdate();
				treeView.Nodes.Add(RootNode);
				treeView.EndUpdate();
			}
		}

		private void ThreadSafeLogOutputAppendText(String text)
		{
			if (LogOutput.InvokeRequired)
			{
				Invoke(new SetTextCallback(ThreadSafeLogOutputAppendText), text);
			}
			else
			{
				LogOutput.AppendText(text);
			}
		}

		private void ThreadSafeLogOutputSetText(String text)
		{
			if (LogOutput.InvokeRequired)
			{
				Invoke(new SetTextCallback(ThreadSafeLogOutputSetText), text);
			}
			else
			{
				LogOutput.Text = text;
			}
		}

		private void ThreadSafeCurrentStateSetText(String text)
		{
			if (statusStrip1.InvokeRequired)
			{
				Invoke(new SetTextCallback(ThreadSafeCurrentStateSetText), text);
			}
			else
			{
				CurrentStateLabel.Text = text;
			}
		}
		private readonly Dictionary<Type, ToolStripItem> _addCheckMenuItems;

		private delegate void SetTextCallback(String text);
		private delegate void SetNodeCallback(TreeNode Node);

		private void LogParametersToolStripMenuItem_Click(object sender, EventArgs e)
		{
			FormSettings formSettings = new FormSettings();
			formSettings.ShowDialog();
		}

		private void AbortCheck_Click(object sender, EventArgs e)
		{
			backgroundWorker.CancelAsync();
		}
	}
}